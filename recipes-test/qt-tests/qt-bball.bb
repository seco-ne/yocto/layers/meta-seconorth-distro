SUMMARY = "BBall based on QML"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "qtbase qtquickcontrols"

inherit qmake5

SRC_URI = "file://bball"

S = "${WORKDIR}/bball"

FILESEXTRAPATHS:prepend := "${THISDIR}:"

QMAKE_PROFILES = "${S}/bball.pro"

pkg_postinst_ontarget:${PN} () {

	# The second instance needs to be a hardlink rather then a softlink
	ln /usr/bin/bball /usr/bin/bball2

}
