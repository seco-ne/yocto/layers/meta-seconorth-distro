#include <QScreen>
#include <QTime>
#include <QDebug>
#include "dialog.h"

Dialog::Dialog(QWidget *parent) :
	QDialog(parent,Qt::FramelessWindowHint)
{
	setupUi(this);
	QScreen *screen = QApplication::screens().at(0);
	move(0,0);
	resize(screen->size());
	label->resize(screen->size());
	m_MouseDownTime = 0;
}

void Dialog::mousePressEvent(QMouseEvent *event)
{
	m_MouseDownTime = QTime::currentTime().msecsSinceStartOfDay();
	label->setText(QString("PRESS\n%1 x %2").arg(event->pos().x()).arg(event->pos().y()));
	printf("\n\n%8d  PRESS   %3d x %3d\n",0,event->pos().x(),event->pos().y());
}

void Dialog::mouseReleaseEvent(QMouseEvent *event)
{
	int t = QTime::currentTime().msecsSinceStartOfDay() - m_MouseDownTime;
	label->setText(QString("RELEASE\n%1 x %2").arg(event->pos().x()).arg(event->pos().y()));
	printf("%8d  RELEASE %3d x %3d\n",t,event->pos().x(),event->pos().y());
}

void Dialog::mouseMoveEvent(QMouseEvent *event)
{
	int t = QTime::currentTime().msecsSinceStartOfDay() - m_MouseDownTime;
	label->setText(QString("MOVE\n%1 x %2").arg(event->pos().x()).arg(event->pos().y()));
	printf("%8d  MOVE    %3d x %3d\n",t,event->pos().x(),event->pos().y());
}

void Dialog::on_pushButtonQuit_clicked()
{
	printf("on_pushButtonQuit_clicked\n");
	qApp->quit();
}

void Dialog::on_pushButtonTouch_clicked()
{
	const QString touch=QString("get touch events");
	const QString mouse=QString("get mouse events");

	if( 0 == pushButtonTouch->text().compare(touch))
	{
		printf("on_pushButtonTouch_clicked - touch\n");
		setAttribute(Qt::WA_AcceptTouchEvents, true);
		pushButtonTouch->setText(mouse);
	}else{
		printf("on_pushButtonTouch_clicked - mouse\n");
		setAttribute(Qt::WA_AcceptTouchEvents, false);
		pushButtonTouch->setText(touch);
	}
}

bool Dialog::event(QEvent *event)
{
	int t = QTime::currentTime().msecsSinceStartOfDay() - m_MouseDownTime;
	int cnt=0;
	char * state;
	switch (event->type()) {
	case QEvent::TouchBegin:
	case QEvent::TouchUpdate:
	case QEvent::TouchEnd:
		{
			QTouchEvent *touch = static_cast<QTouchEvent *>(event);
			QList<QTouchEvent::TouchPoint> touchPoints = static_cast<QTouchEvent *>(event)->touchPoints();
			foreach (const QTouchEvent::TouchPoint &touchPoint, touchPoints) {
				switch (touchPoint.state()) {
				case Qt::TouchPointPressed:
					state="Pressed";
					break;
				case Qt::TouchPointMoved:
					state="Moved";
					break;
				case Qt::TouchPointStationary:
					state="Stationary";
					break;
				case Qt::TouchPointReleased:
					state="Released";
					break;
				}
				printf("%8d  TP %d %d - %10s    %3d x %3d\n",t, cnt, touchPoint.id(), state, (int) touchPoint.pos().x(), (int) touchPoint.pos().y());
				++cnt;
			}
			break;
		}
	default:
		return QWidget::event(event);
	}
	return true;
}
