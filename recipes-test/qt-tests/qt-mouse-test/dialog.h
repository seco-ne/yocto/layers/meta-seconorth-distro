#ifndef DIALOG_H
#define DIALOG_H
#include <QMouseEvent>
#include "ui_dialog.h"

class Dialog : public QDialog, private Ui::Dialog
{
  Q_OBJECT

public:
  explicit Dialog(QWidget *parent = 0);
protected:
  virtual void mousePressEvent(QMouseEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual bool event(QEvent *event);
private slots:
  void on_pushButtonQuit_clicked();
  void on_pushButtonTouch_clicked();

private:
  int m_MouseDownTime;
};

#endif // DIALOG_H
