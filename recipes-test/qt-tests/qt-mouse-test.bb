SUMMARY = "SECO Northern Europe Qt Mouse Test"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "qtbase"

USE_X11 = "${@base_contains("DISTRO_FEATURES", "x11", "yes", "no", d)}"

require recipes-qt/qt5/qt5.inc

FILESEXTRAPATHS:prepend := "${THISDIR}:"

SRC_URI = " \
    file://qt-mouse-test \
    "

S = "${WORKDIR}/qt-mouse-test"
PR = "r0"

QMAKE_PROFILES = "${S}/qt-mouse-test.pro"

do_install () {
	# Application
	install -d 0644 ${D}${bindir}
	install -m 0755 qt-mouse-test ${D}${bindir}
}
