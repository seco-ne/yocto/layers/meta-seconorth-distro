SUMMARY = "SECO Northern Europe Qt Sound Test"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "qtbase qtmultimedia"

require recipes-qt/qt5/qt5.inc

FILESEXTRAPATHS:prepend := "${THISDIR}:"

SRC_URI = " \
	file://qt-playsound \
	"

S = "${WORKDIR}/qt-playsound"

QMAKE_PROFILES = "${S}/qt-playsound.pro"

do_install () {
	# application
	install -d 0644 ${D}${bindir}
	install -m 0755 qt-playsound ${D}${bindir}

	# audio files
	install -d 0644 ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/1.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/2.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/3.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/4.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/5.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/6.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/7.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/8.ogg ${D}${datadir}/sounds/ogg
	install -m 0755 ${S}/audiofiles/9.ogg ${D}${datadir}/sounds/ogg
}
