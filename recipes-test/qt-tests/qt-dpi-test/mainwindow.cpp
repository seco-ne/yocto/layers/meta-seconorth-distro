#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QScreen>
#include <QDebug>

// Helper function to return display orientation as a string.
QString Orientation(Qt::ScreenOrientation orientation)
{
    switch (orientation) {
        case Qt::PrimaryOrientation           : return "Primary";
        case Qt::LandscapeOrientation         : return "Landscape";
        case Qt::PortraitOrientation          : return "Portrait";
        case Qt::InvertedLandscapeOrientation : return "Inverted landscape";
        case Qt::InvertedPortraitOrientation  : return "Inverted portrait";
        default                               : return "Unknown";
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    ui->setupUi(this);

    getScreenSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QApplication::quit();
}


void MainWindow::on_pushButton_2_clicked()
{
    getScreenSettings();

}

void MainWindow::getScreenSettings()
{
    QString outtext = QString("");
    outtext.append( QString("Number of screens: %1 ").arg( QGuiApplication::screens().size()));


    outtext.append(QString("\nPrimary screen: %1").arg(QGuiApplication::primaryScreen()->name()));

    foreach (QScreen *screen, QGuiApplication::screens()) {
        outtext.append("\nInformation for screen: "  +  screen->name());
        outtext.append(QString("\n  Available geometry: %1 %2 %3 x %4"  )
                .arg( screen->availableGeometry().x())
                .arg( screen->availableGeometry().y())
                .arg( screen->availableGeometry().width())
                .arg( screen->availableGeometry().height()));
        outtext.append( QString("\n  Available size: %1 x %2"  )
                .arg(  screen->availableSize().width())
                .arg(  screen->availableSize().height()));
        outtext.append( QString("\n  Available virtual geometry:  %1 %2 %3 x %4"  )
                .arg(  screen->availableVirtualGeometry().x())
                .arg(  screen->availableVirtualGeometry().y())
                .arg(  screen->availableVirtualGeometry().width())
                .arg(  screen->availableVirtualGeometry().height()));
        outtext.append( QString("\n  Available virtual size: %1 x %2"  )
                .arg(  screen->availableVirtualSize().width())
                .arg(  screen->availableVirtualSize().height()));
        outtext.append( QString("\n  Depth: %1 bits"  ).arg(  screen->depth()));
        outtext.append( QString("\n  Geometry:   %1 %2 %3 x %4"  )
                .arg(  screen->geometry().x()  )
                .arg(  screen->geometry().y()  )
                .arg(  screen->geometry().width()  )
                .arg(  screen->geometry().height()));
        outtext.append( QString("\n  Logical DPI: %1"  ).arg(  QString::number(screen->logicalDotsPerInch())));
        outtext.append( QString("\n  Logical DPI X %1: "  ).arg( QString::number( screen->logicalDotsPerInchX())));
        outtext.append( QString("\n  Logical DPI Y: %1"  ).arg( QString::number( screen->logicalDotsPerInchY())));
        outtext.append( QString("\n  Orientation: %1"  ).arg(  Orientation(screen->orientation())));
        outtext.append( QString("\n  Physical DPI: %1"  ).arg(  QString::number(screen->physicalDotsPerInch())));
        outtext.append( QString("\n  Physical DPI X: %1"  ).arg(QString::number(  screen->physicalDotsPerInchX())));
        outtext.append( QString("\n  Physical DPI Y: %1"  ).arg( QString::number( screen->physicalDotsPerInchY())));
        outtext.append( QString("\n  Physical size: %1 x %2 mm"  )
                .arg( QString::number( screen->physicalSize().width()))
                .arg( QString::number( screen->physicalSize().height())));
        outtext.append( QString("\n  Primary orientation: %1"  ).arg(  Orientation(screen->primaryOrientation())));
        outtext.append( QString("\n  Refresh rate: %1 Hz"  ).arg( QString::number( screen->refreshRate() ) ));
        outtext.append( QString("\n  Size: %1 x %2"  ).arg(  screen->size().width()  ).arg(  screen->size().height()));
        outtext.append( QString("\n  Virtual geometry: %1 %2 %3 x %4"  )
                .arg(  screen->virtualGeometry().x())
                .arg(  screen->virtualGeometry().y()  )
                .arg(  screen->virtualGeometry().width()  )
                .arg(  screen->virtualGeometry().height()));
        outtext.append( QString("\n  Virtual size: %1 x %2"  )
                .arg(  screen->virtualSize().width()  )
                .arg(  screen->virtualSize().height()));
    }

    qDebug() << outtext;
    ui->textEdit->setText(outtext);
}
