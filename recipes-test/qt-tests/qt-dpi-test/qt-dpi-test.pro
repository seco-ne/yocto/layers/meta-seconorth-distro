#-------------------------------------------------
#
# Project created by QtCreator 2015-02-24T10:23:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt_dpi_test
TEMPLATE = app

# fuer remote debugging sind diese zwei Zeilen erforderlich
target.path = /tmp
INSTALLS   += target

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
