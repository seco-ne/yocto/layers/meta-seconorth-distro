SUMMARY = "Garz & Fricke Qt DPI Test Application"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "qtbase"

USE_X11 = "${@base_contains("DISTRO_FEATURES", "x11", "yes", "no", d)}"

require recipes-qt/qt5/qt5.inc

FILESEXTRAPATHS:prepend := "${THISDIR}:"

SRC_URI = " \
    file://qt-dpi-test \
    "

S = "${WORKDIR}/qt-dpi-test"
PR = "r0"

QMAKE_PROFILES = "${S}/qt-dpi-test.pro"
PDATA_DIR = "${datadir}/${PN}"

do_install () {
	# Application
	install -d ${D}${bindir}
	install -m 0755 qt_dpi_test ${D}${bindir}
}
