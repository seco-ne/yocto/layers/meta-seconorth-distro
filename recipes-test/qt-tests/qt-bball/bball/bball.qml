import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

ApplicationWindow {

    title: "Bouncing Ball"
    visibility: "FullScreen"
    visible: true
    id: main
    height: Screen.height; width: Screen.width

    Image {
        source: "file://opt/ltp/images/itu-r-bt1729-colorbar-16x9.png"
        fillMode: Image.Stretch
        width: parent.width
        height: parent.height
    }

    Rectangle {
        width: 50; 
        height: width

        x: main.width/2; y: main.height/2
        color: "forestgreen"
        id: ball

        border.color: "black"
        border.width: 1
        radius: width*0.5

        //=================================================
        //  Coloranimation
        //=================================================
        SequentialAnimation on color {
            loops: Animation.Infinite
            ColorAnimation { to: "yellow"; duration: 1000 }
            ColorAnimation { to: "red"; duration: 1000 }
            ColorAnimation { to: "green"; duration: 1000 }
            ColorAnimation { to: "blue"; duration: 1000 }
        }

        //=================================================
        //  Animation 
        //=================================================

        Behavior on y { SpringAnimation{ velocity: 1400; } }
        Behavior on x { SpringAnimation{ velocity: 1400; } }
        Component.onCompleted: { 
            y = main.height - ball.height; // start the ball motion
            x = main.width - ball.width; // start the ball motion
        }

        // Detect the ball hitting the top or bottom of the view and bounce it
        onYChanged: {
            if (y <= 0) {
                y = main.height - ball.height;
            } else if (y >= main.height - ball.height) {
                y = 0;
            }
        }
        onXChanged: {
            if (x <= 0) {
                x = main.width - ball.width;
            } else if (x >= main.width - ball.width) {
                x = 0;
            }
        }
        //=================================================
    }
}
