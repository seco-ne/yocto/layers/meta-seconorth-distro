#ifndef __MAIN_H__
#define __MAIN_H__

#include <QCoreApplication>
#include <QMediaPlayer>

class MediaStateHandler : public QObject {
    Q_OBJECT

public:
    QMediaPlayer *player;
    QMediaPlayer::State oldState;
    QCoreApplication *application;
    MediaStateHandler(QMediaPlayer *mpl, QCoreApplication *app);
    ~MediaStateHandler();

public slots:
    void playerStateChanged(QMediaPlayer::State newState);
};

#endif // __MAIN_H__
