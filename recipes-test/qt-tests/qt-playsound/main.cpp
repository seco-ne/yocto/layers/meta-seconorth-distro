#include <QCoreApplication>
#include <QMediaPlayer>
#include <QMediaPlaylist>

#include "main.h"

MediaStateHandler::MediaStateHandler(QMediaPlayer * mpl, QCoreApplication * app)
	: player(mpl)
	, oldState(mpl->state())
	, application(app)
{
}

MediaStateHandler::~MediaStateHandler()
{
}

void MediaStateHandler::playerStateChanged(QMediaPlayer::State newState) {

	if (oldState == QMediaPlayer::PlayingState && newState == QMediaPlayer::StoppedState)
	{
		application->quit();
	}
	oldState = newState;
}

int main(int argc, char *argv[])
{
	QCoreApplication app(argc, argv);

	QMediaPlayer player(0, QMediaPlayer::LowLatency);
	QMediaPlaylist playlist;
	player.setPlaylist(&playlist);

	// add all command line arguments to the playlists
	QListIterator<QString> i(app.arguments().mid(1));
	while (i.hasNext())
		playlist.addMedia(QUrl::fromLocalFile(i.next()));

	// application, mediaplayer and the connection of both
	MediaStateHandler msh (&player, &app);

	msh.connect(&player, SIGNAL(stateChanged(QMediaPlayer::State)),
			&msh, SLOT(playerStateChanged(QMediaPlayer::State)));

	player.play();

	// This starts the event loop of the application:
	return app.exec();
}
