SUMMARY = "SECO Northern Europe EMC Testsuite"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/gf-emc-test-suite"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SDL_DEPENDS = "libsdl libsdl-image libsdl-gfx libinput libevdev"
X11_DEPENDS = "${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'xrandr virtual/libgl', '', d)}"

DEPENDS += " \
    ${SDL_DEPENDS} \
    ${X11_DEPENDS} \
    alsa-lib \
    alsa-utils \
    libgpiod \
    libpthread-stubs \
    libsocketcan \
    ltp \
    ncurses \
    virtual/kernel \
"

RDEPENDS:${PN} += " \
    bash \
    bluez5 \
    expect \
    fbv \
    glibc-gconv-utf-16 \
    packagegroup-tools-bluetooth \
    qt-bball \
    qtbase-fingerpaint \
    stress \
"

SRCREV = "${AUTOREV}"
SRC_URI = " \
    git://git.seco.com/seco-ne/tools/gf-emc-test-suite.git;protocol=https;branch=dunfell;nobranch=1 \
"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git/testcases"

PREFIX="/opt/ltp"
export prefix = "${PREFIX}"
export exec_prefix = "${PREFIX}"

EXTRA_OEMAKE += " 'SYSROOT=${STAGING_DIR_TARGET}' "

TARGET_CC_ARCH += "-pthread"
CFLAGS:prepend = " -I${STAGING_DIR_TARGET}/opt/ltp/include -DLTP_VERSION=20200120"
CFLAGS:append = " -Wno-error=unused-result -Wno-error=format= -Wno-error=format-security -Wno-error=stringop-overflow= -Wno-error=int-to-pointer-cast -Wfatal-errors"
PV = "1.0"

LDFLAGS:prepend = " -L${STAGING_DIR_TARGET}/opt/ltp/lib "

CLEANBROKEN = "1"

WAYLAND_ENABLED = "${@bb.utils.contains("DISTRO_FEATURES", "wayland", "yes", "no", d)}"
X11_ENABLED = "${@bb.utils.contains("DISTRO_FEATURES", "x11", "yes", "no", d)}"

do_compile () {
    oe_runmake all_tests

    # Currently the x11 tests do not work with xwayland, disable them for now
    if [ "${X11_ENABLED}" = "yes" &&  "${WAYLAND_ENABLED}" = "no"]; then
        oe_runmake all_tests_x11
    fi
    oe_runmake all_tests_sdl
}

do_install () {
    install -d ${D}${PREFIX}/testcases/bin

    install -m 0755 ${S}/bin/* ${D}${PREFIX}/testcases/bin
    cp -rd ${S}/../images ${D}${PREFIX}
    cp -rd ${S}/../runtest ${D}${PREFIX}
    cp -rd ${S}/../scripts ${D}${PREFIX}
    chmod +x ${D}${PREFIX}/scripts/*
}

FILES:${PN} = " \
    ${PREFIX}/testcases/bin/* \
    ${PREFIX}/images \
    ${PREFIX}/runtest \
    ${PREFIX}/runtest/scripts \
    ${PREFIX}/scripts \
"

FILES:${PN}-dbg = " \
    ${PREFIX}/testcases/bin/.debug/ \
"

FILES:${PN}-dev = " \
    /usr/src/ \
"

pkg_postinst_ontarget:${PN} () {
    # The second instance needs to be a hardlink rather then a softlink
    ln /usr/bin/fingerpaint /usr/bin/fingerpaint2
}

