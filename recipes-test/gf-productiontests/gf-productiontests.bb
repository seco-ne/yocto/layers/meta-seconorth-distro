SUMMARY = "SECO North Linux Production Test Project"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/gf-productiontests"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=a23a74b3f4caf9616230789d94217acb"

DEPENDS += "tslib libdrm"

SRC_URI = " \
    git://git.seco.com/seco-ne/tools/gf-productiontests.git;protocol=https;branch=dunfell;nobranch=1 \
    file://dualscreen-pixeltest.sh \
    file://dualscreen-touchtest.sh \
    "
SRCREV = "${AUTOREV}"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

inherit autotools

do_configure:prepend() {
    #The autogen.sh script needs to be run in and from the source directory
    ( cd ${S}; ./autogen.sh )
}

do_install () {
    install -d ${D}/opt/productiontests/bin
    install -m 0755 ${B}/guf_pixeltest ${D}/opt/productiontests/bin/
    install -m 0755 ${B}/guf_touchtest ${D}/opt/productiontests/bin/
    install -m 0755 ${B}/drm/drm_pixeltest ${D}/opt/productiontests/bin/
    install -m 0755 ${B}/drm/drm_touchtest ${D}/opt/productiontests/bin/

    install -m 0755 ${WORKDIR}/dualscreen-pixeltest.sh ${D}/opt/productiontests/bin/
    install -m 0755 ${WORKDIR}/dualscreen-touchtest.sh ${D}/opt/productiontests/bin/
}

FILES:${PN} = " \
    /opt/productiontests/* \
"
