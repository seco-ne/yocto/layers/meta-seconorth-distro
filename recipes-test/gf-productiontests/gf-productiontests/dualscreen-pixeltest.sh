#!/bin/sh
HELP="
# Run screen test on dual-screen architecture.
# The screens are tested sequentially.
#
# Options:
#         -l|--left            Run test only on the left screen
#         -r|--right           Run test only on the right screen
"


TEST="/opt/productiontests/bin/guf_pixeltest"

# i2c mapping for the dual-espresso board
TOUCH1_I2C="5-002a"
TOUCH2_I2C="0-002a"

TOUCH1_EVENT="$(find /sys/bus/i2c/drivers/egalax_i2c/${TOUCH1_I2C}/input/input*/ \
		-name "event*" -maxdepth 1 -type d -exec basename {} \;)"
TOUCH2_EVENT="$(find /sys/bus/i2c/drivers/egalax_i2c/${TOUCH2_I2C}/input/input*/ \
		-name "event*" -maxdepth 1 -type d -exec basename {} \;)"

BL1_POWER="/sys/class/backlight/backlight1/bl_power"
BL2_POWER="/sys/class/backlight/backlight2/bl_power"

LEFT=false
RIGHT=false

exit_failed () {
	echo 0 > ${BL1_POWER}
	echo 0 > ${BL2_POWER}
	echo "Failed"
	exit 1
}

parse_args () {
	while [ $# -ge 1 ]; do
		case $1 in
			-l|--left)
				LEFT=true
				;;
			-r|--right)
				RIGHT=true
				;;
			*)
				echo "$HELP" | cut -b 3-
				exit 1
				;;
		esac
		shift
	done

	# Test both screens, if neither left nor right is set
	if (! ${LEFT} && ! ${RIGHT})
	then
		LEFT=true
		RIGHT=true
	fi
}


parse_args "$@"

# Screen 1
if ${LEFT}
then
	echo 1 > ${BL1_POWER}
	TSLIB_TSDEVICE=/dev/input/${TOUCH1_EVENT} ${TEST} || exit_failed
	echo 0 > ${BL1_POWER}
fi

# Screen 2
if ${RIGHT}
then
	echo 1 > ${BL2_POWER}
	TSLIB_TSDEVICE=/dev/input/${TOUCH2_EVENT} ${TEST} || exit_failed
	echo 0 > ${BL2_POWER}
fi

echo "pixeltest done"

exit 0
