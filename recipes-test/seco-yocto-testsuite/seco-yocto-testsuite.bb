SUMMARY = "SECO Yocto test suite"
DESCRITPTION = "Collection of scripts and small programms for various feature tests"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/yocto-test-suite"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRCREV = "${AUTOREV}"
SRC_URI = " \
  git://git.seco.com/seco-ne/tools/yocto-test-suite.git;protocol=https;branch=main;nobranch=1 \
"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

RDEPENDS:${PN} = " \
    jq \
    qmlbench \
"

S = "${WORKDIR}/git"

do_install () {
    make install
    mkdir -p ${D}/opt/yocto-testsuite
    cp -rv out/* ${D}/opt/yocto-testsuite
}

FILES:${PN} = " \
    /opt/yocto-testsuite/* \
"



