SUMMARY = "SECO certification tools"
DESCRIPTION = "Collection of scripts and small programs for rf-testing certifertification tests"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/yocto-test-suite"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRCREV = "e2455ea01218e6a07a87d96bcc06358dca70fa4c"
SRC_URI = " \
  git://git@git.seco.com/seco-ne/infrastructure/certification-tools.git;protocol=https;branch=mast;nobranch=1 \
"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

RDEPENDS:${PN} = " \
    bash \
    firmware-nxp-wifi-nxp8997-pcie \
    kernel-module-nxp-wlan \
    "

S = "${WORKDIR}/git"

do_install(){
    install -d ${D}/opt/rf-test/azurewave

    install -m 0755 ${B}/AZUREWAVE/scripts_TR8MP/*.sh \
                    ${D}/opt/rf-test/azurewave/
}

FILES:${PN} = " \
    /opt/rf-test/azurewave/* \
"


