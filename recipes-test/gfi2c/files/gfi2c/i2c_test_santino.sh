#/bin/bash

# PMIC PFUZE3000
(while : ; do gfi2c --test /dev/i2c-0 0x08 0x1c 0xff 0x1d 0xff -n 4000 -v; done) &

# SGTL5000
(while : ; do gfi2c --test /dev/i2c-0 0x0a 0x0120 0xffff 0x0122 0xffff -n 4000 -v -r 16 -w 16; done) &

# LM75
(while : ; do gfi2c --test /dev/i2c-0 0x49 0x3 0xff80 0x2 0xff80 -n 4000 -v -w 16; done) &

# RTC PCF8563
(while : ; do gfi2c --test /dev/i2c-0 0x51 0xa 0x3f 0xb 0x3f -n 4000 -v; done) &

# Touch SIS9255

# EEPROM M24C32 (This test writes 'SECO Northern Europe' to EEPROM reads it back and compares it in a loop)
# NOTE: THIS TEST WILL DESTROY THE EEPROM IF RUNNING EXCESSIVELY, USE IT JUST A FEW SECONDS
#(while : ; do \
#  dd if=/dev/zero of=/sys/bus/i2c/devices/0-0050/eeprom bs=1 count=64 >& /dev/null; \
#  echo "SECO Northern Europe" > /tmp/eeprom_in.txt; \
#  dd if=/tmp/eeprom_in.txt of=/sys/bus/i2c/devices/0-0050/eeprom bs=1 count=21 >& /dev/null; \
#  dd if=/sys/bus/i2c/devices/0-0050/eeprom of=/tmp/eeprom_out.txt bs=1 count=21 >& /dev/null; \
#  diff /tmp/eeprom_in.txt /tmp/eeprom_out.txt; \
#  cmp /tmp/eeprom_in.txt /tmp/eeprom_out.txt || echo "EEPROM error: " && diff /tmp/eeprom_in.txt /tmp/eeprom_out.txt; \
#done) &

# Provoke I2C timeout errors due to CPU load.
# If this test fails, i2c-imx GUF changes has to be applied to the driver.
stress-ng --cpu 4

