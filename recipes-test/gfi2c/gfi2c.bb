SUMMARY = "SECO Northern Europe I2C test tool"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS += "virtual/kernel"

SRC_URI = " \
  file://gfi2c \
"

PE="1"
PR = "r1"

S = "${WORKDIR}/gfi2c"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 gfi2c ${D}${bindir}
	install -d ${D}${datadir}/test/i2c
	install -m 0755 i2c_test_santino.sh ${D}${datadir}/test/i2c
}

PARALLEL_MAKE = ""

FILES:${PN}:append = " \
  ${datadir} \
  ${datadir}/test \
  ${datadir}/test/i2c \
  ${datadir}/test/i2c/i2c_test_santino.sh \
"
