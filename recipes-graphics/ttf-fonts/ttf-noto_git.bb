SUMMARY = "Google Noto Fonts"
HOMEPAGE = "https://fonts.google.com/noto"
SECTION = "fonts"
LICENSE = "OFL-1.1"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=fce5baa9b16328f04e2afc29f6e4e882"

INHIBIT_DEFAULT_DEPS = "1"

inherit allarch fontcache

SRC_URI = " \
    git://github.com/google/fonts.git;protocol=https;branch=main \
    file://LICENSE \
"

SRCREV = "e69986d37832cc07980dee9d70fa5597b0b47ad8"

# SC = Simplified Chinese
# TC = Traditional Chinese
# JP = Japanese
# KR = Korean
# TH = Thai

S = "${WORKDIR}/git/ofl"

PACKAGES = " \
    ttf-noto \
    ttf-noto-sc \
    ttf-noto-tc \
    ttf-noto-jp \
    ttf-noto-kr \
    ttf-noto-th \
"

FONT_PACKAGES = " \
    ttf-noto \
    ttf-noto-sc \
    ttf-noto-tc \
    ttf-noto-jp \
    ttf-noto-kr \
    ttf-noto-th \
"

FILES:${PN} = "${datadir}/fonts/ttf/NotoSans-*.ttf"
FILES:${PN}-sc = "${datadir}/fonts/otf/NotoSansSC-Regular.otf"
FILES:${PN}-tc = "${datadir}/fonts/otf/NotoSansTC-Regular.otf"
FILES:${PN}-jp = "${datadir}/fonts/otf/NotoSansJP-Regular.otf"
FILES:${PN}-kr = "${datadir}/fonts/otf/NotoSansKR-Regular.otf"
FILES:${PN}-th = "${datadir}/fonts/ttf/NotoSansThaiLooped-Regular.ttf"

do_install() {
    install -d ${D}${datadir}/fonts/ttf/
    install -d ${D}${datadir}/fonts/otf/

    find ./notosans -name '*.ttf' -exec install -m 0644 {} ${D}${datadir}/fonts/ttf/ \;

    install -m 0644 ${S}/notosanssc/NotoSansSC-Regular.otf ${D}${datadir}/fonts/otf
    install -m 0644 ${S}/notosanstc/NotoSansTC-Regular.otf ${D}${datadir}/fonts/otf
    install -m 0644 ${S}/notosansjp/NotoSansJP-Regular.otf ${D}${datadir}/fonts/otf
    install -m 0644 ${S}/notosanskr/NotoSansKR-Regular.otf ${D}${datadir}/fonts/otf
    install -m 0644 ${S}/notosansthailooped/NotoSansThaiLooped-Regular.ttf ${D}${datadir}/fonts/ttf
}

