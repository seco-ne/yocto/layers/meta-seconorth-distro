SUMMARY = "Framebuffer Image Viewer"
HOMEPAGE = "http://www.eclis.ch/fbv/"
SECTION = "graphics"

inherit autotools-brokensep

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=130f9d9dddfebd2c6ff59165f066e41c"

DEPENDS = "libpng jpeg"
INHIBIT_PACKAGE_STRIP = "1"

SRC_URI = " \
    git://repo.or.cz/fbv.git;protocol=https;branch=master\
    file://remove-hardcoded-cc-from-make.patch \
    file://use-giflib.patch \
"

SRCREV = "127dd84cede022cd5173ff2d7450677a14486784"

S = "${WORKDIR}/git"

do_configure() {
    ${B}/configure --prefix=/usr --bindir=${bindir} --without-bmp --without-libgif
}

do_install:prepend() {
    install -d ${D}${bindir}
    install -d ${D}/usr/man/man1
}

FILES:${PN}-doc = " \
    /usr/man/man1/${PN}.1.gz \
"
