SUMMARY = "Wayland VNC Client"
DESCRIPTION = "This is a work-in-progress implementation of a Wayland native VNC client."

LICENSE = "GPL-2.0-only & ISC"
LIC_FILES_CHKSUM = " \
    file://COPYING.GPL;md5=361b6b837cad26c6900a926b62aada5f \
    file://COPYING;md5=94fc374e7174f41e3afe0f027ee59ff7 \
"

inherit meson pkgconfig

DEPENDS = "wayland wayland-native wayland-protocols libxkbcommon pixman libdrm virtual/libgbm virtual/egl virtual/libgles2 ffmpeg lzo gnutls libgcrypt jpeg"

SRC_URI = "\
    git://github.com/any1/wlvncc.git;protocol=https;branch=master;name=wlvncc \
    git://github.com/any1/aml.git;protocol=https;branch=master;destsuffix=git/subprojects/aml;name=aml \
    file://0001-meson.build-use-native-wayland-scanner.patch \
    file://0002-xdg-v4.patch \
    file://0003-wayland-version.patch \
    file://0004-pointer.patch \
    file://0005-dma-format.patch \
    file://0006-add-plain-password-argument.patch \
"
SRCREV_wlvncc = "2b9a886edd38204ef36e9f9f65dd32aaa3784530"
SRCREV_aml = "ede424968e9e521c6e058be48d3ac89e8c9d700b"

S = "${WORKDIR}/git"
