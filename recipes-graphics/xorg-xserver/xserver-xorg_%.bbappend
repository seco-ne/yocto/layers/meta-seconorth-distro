# The patch was added to the upstream version of x11 but hasn't been
# removed from meta-freescale. Since it throws an error during the build,
# we remove it here until the patch has been removed from the freescale
# layer.
#
# Also see https://gitlab.freedesktop.org/xorg/xserver/-/commit/
# 36e353bcf428c4e6a31292ffa749ea6395cba4a3

SRC_URI:remove = " \
    file://0003-Remove-check-for-useSIGIO-option.patch \ 
"

# Remove patches, that are drawn from meta-freescale.
# There is a version mismatch and they can not be applied.
SRC_URI:remove = " \
    file://0001-MGS-5186-Per-Specification-EGL_NATIVE_PIXMAP_KHR-req.patch \
    file://0001-glamor-glamor_egl.c-EGL_NATIVE_PIXMAP_KHR-do-not-req.patch \
    file://0001-prefer-to-use-GLES2-for-glamor-EGL-config.patch \
    file://0001-hw-xwayland-Makefile.am-fix-build-without-glx.patch \
    file://0001-xfree86-define-FOURCC_NV12-and-XVIMAGE_NV12.patch \
    file://0002-glamor-add-support-for-GL_RG.patch \
    file://0003-glamor-add-support-for-NV12-in-Xv.patch \
    file://0004-glamor-Remove-unused-format_for_pixmap-helper.patch \
    file://0005-glamor-Stop-trying-to-store-the-pixmap-s-format-in-g.patch \
    file://0006-glamor-Plumb-the-pixmap-through-fbo-creation-instead.patch \
    file://0007-glamor-Switch-the-gl_flavor-to-a-boolean-is_gles.patch \
    file://0008-glamor-Introduce-a-central-place-for-our-pixmap-form.patch \
"
