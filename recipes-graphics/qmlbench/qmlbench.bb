# WARNING: the following LICENSE and LIC_FILES_CHKSUM values are best guesses - it is
# your responsibility to verify that the values are complete and correct.
#
# The following license files were not able to be identified and are
# represented as "Unknown" below, you will need to check them yourself:
#   LICENSE.GPL3-EXCEPT
#
# NOTE: multiple licenses have been detected; they have been separated with &
# in the LICENSE value for now since it is a reasonable assumption that all
# of the licenses apply. If instead there is a choice between the multiple
# licenses then you should change the value to separate the licenses with |
# instead of &. If there is any doubt, check the accompanying documentation
# to determine which situation is applicable.
LICENSE = "GPL-3.0-only & The-Qt-Company-GPL-Exception-1.0"
LIC_FILES_CHKSUM = "file://LICENSE.GPL3-EXCEPT;md5=763d8c535a234d9a3fb682c7ecb6c073 \
                    file://LICENSE.GPLv3;md5=d32239bcb673463ab874e80d47fae504"

SRC_URI = "git://github.com/CrimsonAS/qmlbench.git;protocol=https;nobranch=1"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "55c0c1b9909fde5008015dee3cf8661a41a983ba"

S = "${WORKDIR}/git"

DEPENDS = "qtbase qtquickcontrols2"
inherit qmake5

do_configure:prepend() {
    # Change install path
    find ${S} -iname '*.pro' -exec sed -i -e  's|/root|/opt/qmlbench|' {} \;
}

FILES:${PN} = "/opt/qmlbench/*"

