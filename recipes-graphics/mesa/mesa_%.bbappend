# FIXME: mesa should support 'x11-no-tls' option
python () {
    overrides = d.getVar("OVERRIDES", True).split(":")
    if "imxgpu2d" not in overrides:
        return
    if "imx8m" in overrides:
        return

    x11flag = d.getVarFlag("PACKAGECONFIG", "x11", False)
    d.setVarFlag("PACKAGECONFIG", "x11", x11flag.replace("--enable-glx-tls", "--enable-glx"))
}

# Add dependency so that GLES3 header don't need to be added manually
RDEPENDS:libgles2-mesa-dev += "libgles3-mesa-dev"

# Enable Etnaviv support, imx-drm is covered by kmsro
PACKAGECONFIG:append:mx6 = " gallium kmsro etnaviv"

