SUMMARY = "Drm framebuffer tool, to access the drm output similar to the old fb interface."
HOMEPAGE = "https://embear.ch/blog/drm-framebuffer"

LICENSE = "GPL-3.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=7702f203b58979ebbc31bfaeb44f219c"

SECTION = "graphics"
DEPENDS = "libdrm"

SRCREV = "255fe7a1495fcd398b49343b75fad6565e3264bd"
SRC_URI = " \
    git://github.com/embear-engineering/drm-framebuffer.git;branch=main;protocol=https \
"
SRC_URI[sha256sum] = "4ade75d79dc62268ff3d2b37cfa252a8bb2a74ecca7e1d58a8c5371baf8fb450"

S = "${WORKDIR}/git"

EXTRA_OEMAKE += "CFLAGS='-Wall -I=/usr/include/drm'"
EXTRA_OEMAKE += "LDFLAGS='-ldrm'"
TARGET_CC_ARCH += "${LDFLAGS}"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${PN} ${D}${bindir}
}
