SUMMARY = "Netpbm toolkit for manipulation of graphic images"
DESCRIPTION = "\
    Netpbm is a toolkit for manipulation of graphic images, \
    including conversion of images between a variety of different \
    formats. There are over 300 separate tools in the package \
    including converters for about 100 graphics formats."
AUTHOR = "Bryan Henderson <bryanh@giraffe-data.com>"
HOMEPAGE = "https://netpbm.sourceforge.net/"
SECTION = "graphics"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "\
    file://doc/copyright_summary;md5=0a5d51ab9c96cf1d16a4fc492adc4618 \
    file://doc/GPL_LICENSE.txt;md5=079b27cd65c86dbc1b6997ffde902735 \
    file://doc/COPYRIGHT.PATENT;md5=12058e0fe45009031ca66cdc09c838a5 \
    file://converter/ppm/ppmtompeg/COPYRIGHT;md5=524ff2e7d68c9b2b9be44fa6dc43b6a9 \
    file://converter/pbm/pbmtoppa/LICENSE;md5=e6d8a8731b55aec86c981b6474ff0888 \
    file://converter/other/pnmtopalm/LICENSE;md5=6d644cf37a0fd379433b17c0fefc4c32 \
    file://converter/other/jbig/libjbig/COPYING;md5=8ca43cbc842c2336e835926c2166c28b \
    file://converter/other/cameratopam/COPYRIGHT;md5=b599b5fdcb221af54d1ae96023826a79 \
    file://lib/util/LICENSE.txt;md5=b0535eb75eda9ffea63e232ae46f4f4b \
    file://other/pamx/COPYRIGHT;md5=70075b407c887a3c57e794e2dbffeb1b"

DEPENDS += "jpeg libpng tiff zlib"

SRC_URI = "\
    https://sourceforge.net/projects/netpbm/files/super_stable/${PV}/netpbm-${PV}.tgz \
    file://config.mk \
    file://oeendiangen \
    file://oetypegen \
    file://0001-pamx-Fix-pkg-config-X11-query.patch \
    file://0002-ximtoppm-Fix-string-copy.patch"
SRC_URI[md5sum] = "c566fc35d34f3de4d8e41a578bc3a898"
SRC_URI[sha1sum] = "c0b69d0c4bb48ede501a2b459a749df049a72431"
SRC_URI[sha256sum] = "a79b86a9ff28bac4fa46688a6cb6e537be0129d23fc74ec71a941b0791679942"
SRC_URI[sha384sum] = "24c3455dbc258ec660dc413e17bac1775714f3f05431ee9eba75dc131db504833e1d4d649ebaf060f838ea6092851a3c"
SRC_URI[sha512sum] = "c66c4a00dc58bd170de35ee1e0c87fea45ca869c2ea05e13487ce1708d7735f5610a6a366ee6fbf4dbd01947b5e21f6217c963893a614a41796629133fd886f7"

SRCREV = "3964"

CVE_PRODUCT = "netpbm"

inherit pkgconfig


PACKAGES =+ "${PN}-lib ${PN}-logotools"

RDEPENDS:${PN}-lib = "perl"
RDEPENDS:${PN}-logotools = "perl ${PN}-lib"
RDEPENDS:${PN} = "perl ${PN}-lib ${PN}-logotools"

BBCLASSEXTEND = "native nativesdk"

EXTRA_OEMAKE = "'ENDIANGEN=${S}/buildtools/oeendiangen' 'TYPEGEN=${S}/buildtools/oetypegen' 'STRIPFLAG=' 'S=${S}' TARGET_LD='${LD}'"

FILES:${PN}-lib = "\
    ${libdir}/*.so.* \
"

FILES:${PN}-logotools = "\
    ${bindir}/pngtopam \
    ${bindir}/pnmalias \
    ${bindir}/pnmcat \
    ${bindir}/pnmcolormap \
    ${bindir}/pnmconvol \
    ${bindir}/pnmcrop \
    ${bindir}/pnmflip \
    ${bindir}/pnmgamma \
    ${bindir}/pnmhisteq \
    ${bindir}/pnmhistmap \
    ${bindir}/pnmindex \
    ${bindir}/pnmindex.sh \
    ${bindir}/pnminvert \
    ${bindir}/pnmmargin \
    ${bindir}/pnmmercator \
    ${bindir}/pnmmontage \
    ${bindir}/pnmnlfilt \
    ${bindir}/pnmnorm \
    ${bindir}/pnmpad \
    ${bindir}/pnmpaste \
    ${bindir}/pnmpsnr \
    ${bindir}/pnmquant \
    ${bindir}/pnmquantall \
    ${bindir}/pnmremap \
    ${bindir}/pnmrotate \
    ${bindir}/pnmscalefixed \
    ${bindir}/pnmshear \
    ${bindir}/pnmsmooth \
    ${bindir}/pnmstitch \
    ${bindir}/pnmtile \
"

do_configure:prepend () {
    install -m755 ${WORKDIR}/oeendiangen ${S}/buildtools/
    install -m755 ${WORKDIR}/oetypegen ${S}/buildtools/
    install -m755 ${WORKDIR}/config.mk ${S}/
}

MAJ_VER = "${@oe.utils.trim_version("${PV}", 2).split('.')[0]}"
MIN_VER = "${@oe.utils.trim_version("${PV}", 2).split('.')[1]}"
LIB_MAJ_VER = "${@int(MAJ_VER) + 1}"

do_install () {
    install -d ${D}${bindir}
    find ${S} \( -name "lib*" -o -name manweb -o -name "*.csh" -o -name makeman -o -path ${S}/buildtools -o -path ${S}/test -o -path ${S}/converter/ppm/ppmtompeg/tst \) -prune -o -type f -executable -exec install -m755 {} ${D}${bindir} \;
    install -d ${D}${libdir}
    find ${S}/lib -name "lib*\.so*" -exec install -m755 {} ${D}${libdir} \;
    ln -srf ${D}${libdir}/libnetpbm.so.${LIB_MAJ_VER}.${MIN_VER} ${D}${libdir}/libnetpbm.so.${LIB_MAJ_VER}
    ln -srf ${D}${libdir}/libnetpbm.so.${LIB_MAJ_VER} ${D}${libdir}/libnetpbm.so
}

