#!/bin/sh

# Checks the current transform value of the primary display (DPI-1 or LVDS-1),
# compares it to the fbcon rotation, and sets the correct transform if the
# rotation in the weston.ini differs from the value of the fbcon rotation.

INI_PATH="/etc/xdg/weston/weston.ini"
ROTATION_FB="0"
ROTATION_WESTON="normal"

if [ -r /sys/class/graphics/fbcon/rotate ]; then
    ROTATION_FB="$(cat /sys/class/graphics/fbcon/rotate)"

    case "$ROTATION_FB" in
        1) ROTATION_WESTON="rotate-90" ;;
        2) ROTATION_WESTON="rotate-180" ;;
        3) ROTATION_WESTON="rotate-270" ;;
    esac
else
    echo "Error: Unable to read fbcon rotation"
    exit 1
fi

echo "Current rotation of fbcon: $ROTATION_FB ($ROTATION_WESTON)"
echo "Checking transform of internal output ..."

export AWK_TODO="check"
export AWK_TARGET_VALUE="$ROTATION_WESTON"

# The script returns 2 in check mode, when
# the rotation doesn't align with the fbcon
# rotation.
/usr/bin/set_transform.awk "$INI_PATH"
ret_awk="$?"

if [ "$ret_awk" -eq 2 ]; then
    echo "Transform differs from fbcon rotation"
    mv "$INI_PATH" "${INI_PATH}.bak"

    export AWK_TODO="set"
    /usr/bin/set_transform.awk "${INI_PATH}.bak" > "$INI_PATH"
elif [ "$ret_awk" -eq 1 ]; then
    echo "Error: Failure while running sub-script"
    exit 1
else
    echo "Transform matches fbcon rotation"
fi

echo "Finished"
echo 0
