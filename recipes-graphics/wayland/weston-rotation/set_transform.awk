#!/usr/bin/awk -f

# The script checks or sets the transform value in the weston.ini. It does so by
# processing line by line, searching for config blocks. Each block starts with
# a [...]. In check mode, the script exits with either 0 (transform fits the
# fbcon rotation) or with 2, if the value differs from the fbcon rotation.
# In set mode, the process_block function prints the blocks to stdout and
# replaces the transform for the primary display with the correct value.

function process_block() {
    if (todo == "check") {
        if (found == 1 && block ~ "transform=" target_value)
            exit 0

    } else {
        if (found == 1)
            sub(/transform=.*/,"transform=" target_value ORS, block)

        printf block
    }
}

# Initialize the script from the ENV parameters
BEGIN {
    todo = ENVIRON["AWK_TODO"]
    target_value = ENVIRON["AWK_TARGET_VALUE"]

    if (length(todo) == 0 || length(target_value) == 0) {
        print "Error: Variables todo and target_value must be set; Exiting ..."
        exit 1
    }

    first_line = 1
}

# If a new block in the weston.ini starts, process the
# current block and reset the runtime values
/^\[/ {
    process_block()
    block = ""
    found = 0
}

# Append every line to the current block value and check
# if the block is the config of the primary output
{
    if (first_line == 1) {
        first_line=0
        block = $0
    } else {
        block = block ORS $0
    }

    if ($0 ~ /name=DPI-1/ || $0 ~ /name=LVDS-1/ || $0 ~ /name=fbdev/)
        found = 1
}

# Process the last block and return the check exit
# value if the transform doesn't match the fbcon
# rotation
END {
    block = block ORS
    process_block()

    if (todo == "check")
        exit 2
}
