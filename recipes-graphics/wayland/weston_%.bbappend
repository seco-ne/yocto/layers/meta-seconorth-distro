FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://profile-weston-touch-calibrator.sh \
    file://0001-xwm-Check-size-hints-in-weston_wm_window_is_position.patch \
"

do_install:append() {
    # There is westen-calibrator and weston-touch-calibrator
    # one should be enough.
    rm ${D}/${bindir}/weston-calibrator

    # Install alias for weston-touch-calibrator so the device does 
    # not has to be specified
    install -d ${D}${sysconfdir}/profile.d/
    install -m 0755 ${WORKDIR}/profile-weston-touch-calibrator.sh ${D}${sysconfdir}/profile.d/weston-touch-calibrator.sh
}

# The display/drm device is called different on the different devices
# here we setup a first boot step to adapt it accordingly
#
# NOTE: The postinst_ontarget is removed for some custom setups
# because it breaks them. Keep this in mind when adding additional
# tasks/steps here.
pkg_postinst_ontarget:${PN}() {
    # Read out the platform as reported by gfplatdetect
    gfplatdetect=$(find /sys/devices -type d -name gfplatdetect )

    if [ -n "$gfplatdetect" ];then
        platform="$(< ${gfplatdetect}/board)"
    else
        platform="$(tr '[:upper:]' '[:lower:]' < /sys/devices/soc0/machine)"
    fi

    case "$platform" in
        santoka)    output="LVDS-1" ;;
        santaro)    output="LVDS-1" ;;
        sanvito)    output="LVDS-1"  ;;
        santvend)   output="LVDS-1"  ;;
        santino)    output="DPI-1"  ;;
        santino-lt) output="DPI-1"  ;;
        *tanaro*)   output="LVDS-1"  ;; # using *x* pattern as soc machine contains a longer string
        nallino)    output="DPI-1"  ;;
        *trizeps8-plus*)  output="LVDS-1" ;;
        *trizeps8-mini*)  output="LVDS-1" ;;
        *"trizeps viii mini"*)  output="LVDS-1" ;;
        *"trizeps viii plus"*)  output="LVDS-1" ;;
        *)          exit 1 ;;
    esac

    # NXP kernel uses framebuffer instead of DRM on i.MX6
    if grep -q -i mxcfb /boot/devicetree.dtb; then
        output="fbdev"
    fi

    sed -i ${sysconfdir}/profile.d/weston-touch-calibrator.sh -e "s/DPI-1/$output/"
}

FILES:${PN}:append = " ${sysconfdir}/profile.d/* "
