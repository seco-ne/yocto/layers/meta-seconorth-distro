FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = "  \
        file://40-libinput-touch-calibration \
        "

RDEPENDS:${PN} += " touchcal-conv "

# The display/drm device is called different on the different devices
# here we setup a first boot step to adapt it accordingly
#
# NOTE: The postinst_ontarget is removed for some custom setups
# because it breaks them. Keep this in mind when adding additional
# tasks/steps here.
pkg_postinst_ontarget:${PN}() {
    # Read out the platform as reported by gfplatdetect
    gfplatdetect=$(find /sys/devices -type d -name gfplatdetect )

    if [ -n "$gfplatdetect" ];then
        platform="$(< ${gfplatdetect}/board)"
    else
        platform="$(tr '[:upper:]' '[:lower:]' < /sys/devices/soc0/machine)"
    fi

    case "$platform" in
        santoka)    output="LVDS-1" ;;
        santaro)    output="LVDS-1" ;;
        sanvito)    output="LVDS-1"  ;;
        santvend)   output="LVDS-1"  ;;
        santino)    output="DPI-1"  ;;
        santino-lt) output="DPI-1"  ;;
        *tanaro*)   output="LVDS-1"  ;; # using *x* pattern as soc machine contains a longer string
        nallino)    output="DPI-1"  ;;
        *trizeps8-plus*)  output="LVDS-1" ;;
        *trizeps8-mini*)  output="LVDS-1" ;;
        *"trizeps viii mini"*)  output="LVDS-1" ;;
        *"trizeps viii plus"*)  output="LVDS-1" ;;
        *)          exit 1 ;;
    esac

    # NXP kernel uses framebuffer instead of DRM on i.MX6
    if grep -q -i mxcfb /boot/devicetree.dtb; then
        output="fbdev"
    fi

    sed -i ${sysconfdir}/xdg/weston/weston.ini -e "s|DPI-1|$output|"

    # Use the rotation value from the kernel commandline also for weston
    if [ -r /sys/class/graphics/fbcon/rotate ]; then
        case "$(cat /sys/class/graphics/fbcon/rotate)" in
            1) ROTATE=rotate-90 ;;
            2) ROTATE=rotate-180 ;;
            3) ROTATE=rotate-270 ;;
            *) ROTATE=normal ;;
        esac
        sed -i ${sysconfdir}/xdg/weston/weston.ini -e "s|^transform=.*\$|transform=$ROTATE|"
    fi
}

do_configure:append() {
    # The graphical.target depends on a display manager which we don't have.
    # Replace it with the multi-user.target.
    sed -i ${WORKDIR}/weston.service -e "s|^WantedBy=.*|WantedBy=multi-user.target|"

    if ${@bb.utils.contains('DISTRO_FEATURES','x11','true','false',d)}; then
        # Disable kiosk-shell
        sed -i ${WORKDIR}/weston.ini -e "s|^shell=kiosk-shell.so.*\$|#shell=kiosk-shell.so|"
        # Enable xwayland
        sed -i ${WORKDIR}/weston.ini -e "s|^#xwayland=true.*\$|xwayland=true|"
    fi
}

do_configure:append:mx6ull() {
    # The graphical.target depends on a display manager which we don't have.
    # Replace it with the multi-user.target.
    sed -i ${WORKDIR}/weston.service -e "s|^WantedBy=.*|WantedBy=multi-user.target|"

    # The i.MX6ULL doesn't have a GPU. Force Weston to use CPU-Rendering.
    sed -i ${WORKDIR}/weston.ini -e "s|^use-pixman=.*|use-pixman=true|"
}

do_install:append() {
    install -d ${D}${sysconfdir}/fng-postinstall/
    install -m 0755 ${WORKDIR}/40-libinput-touch-calibration ${D}${sysconfdir}/fng-postinstall/
}

USERADD_PARAM:${PN} = "--home /home/weston --shell /bin/sh --user-group -G input,video,audio weston"

