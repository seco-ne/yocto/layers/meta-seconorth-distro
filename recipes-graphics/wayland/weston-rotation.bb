SUMMARY = "Helper service that sets the rotation of the internal display according to the fbcon."
HOMEPAGE = "https://seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit systemd

FILESEXTRAPATHS:prepend := "${THISDIR}:${PN}:"

RDEPENDS:${PN} = "busybox"

SRC_URI = " \
    file://set_transform.awk \
    file://weston-rotation.service \
    file://weston_rotation.sh \
"

do_install () {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/set_transform.awk ${D}${bindir}/
    install -m 0755 ${WORKDIR}/weston_rotation.sh ${D}${bindir}/

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/weston-rotation.service ${D}${systemd_system_unitdir}/
}

FILES:${PN} += " \
    ${bindir}/set_transform.sh \
    ${bindir}/weston_rotation.sh \
    ${systemd_system_unitdir}/weston-rotation.service \
"

SYSTEMD_SERVICE:${PN} = "weston-rotation.service"
