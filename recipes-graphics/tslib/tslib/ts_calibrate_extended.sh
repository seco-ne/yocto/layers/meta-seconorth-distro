#!/bin/sh

TS_CALIB_OPTS=""
MODE="calibrate"
ROTATION="0"
POINTERCAL_PATH="/etc/shared/pointercal"
POINTERCAL_TO_UDEVRULE="/etc/fng-postinstall/40-libinput-touch-calibration"

while [ $# -gt 0 ]; do
    case "$1" in
        -t|--min_interval)
            shift
            if ! expr "$1" : '[0-9]*$' > /dev/null; then
                echo "Error: The min_interval must be an integer"
                exit 1
            fi
            TS_CALIB_OPTS=" -t $1 "
            ;;
        gen*)
            # Autogenerate a calibration without actual calibrate
            MODE="generate"
            ;;
        --rotation|-r)
            shift
            ROTATION="$1"
            ;;
        -h|--help)
            echo "Usage: ts_calibrate [generate] OPTIONS"
            echo "    use verb: generate to autocalculate calibration values"
            echo "        -t | --min_interval    minimum time in ms between touch presses"
            echo "        -r | --rotation    [0,90,180,270,INV,CW,CCW,FLIP] rotation of the touch for generation mode"
            exit 0
            ;;
        *)
            echo "Error: Unknown option"
            exit 1
            ;;
    esac
    shift
done

case "$MODE" in
    generate)
        touchcal-conv create-pointercal --touchdevice="$TSLIB_TSDEVICE" --rotation="$ROTATION"
        ;;
    calibrate|*)

     # Stop weston if it is running to get acces to the display
     if command -v systemctl 1>/dev/null;then
         if systemctl is-active weston --quiet;then
             systemctl stop weston --quiet
         fi
     fi

     # Get system rotation from the kernel console
     if [ -r /sys/class/graphics/fbcon/rotate ]; then
         TS_CALIB_OPTS="$TS_CALIB_OPTS -r $(cat /sys/class/graphics/fbcon/rotate) "
     fi

     printf "\n== Start touchscreen calibration ==\n"

     if ! grep -qs '/etc/shared ' /proc/mounts; then
         printf "\nError: Unable to find mounted shared partition at /etc/shared; Stopping ...\n\n"
         exit 1
     fi

     # The ts_calibrate is now rotated according to the fbcon rotation.
     # This results in pointercal, where the 10th parameter is the
     # fbcon rotation. Since this breaks with our current behavior, we
     # replace the rotation value back to normal (= 0).

     TSLIB_CALIBFILE="/tmp/pointercal"
     export TSLIB_CALIBFILE

     printf "\n== Run ts_calibrate ==\n"

     # shellcheck disable=SC2086
     /usr/bin/ts_calibrate $TS_CALIB_OPTS

     awk -F' ' 'BEGIN{OFS=FS} { $10="0" ; printf $f }' /tmp/pointercal > "$POINTERCAL_PATH"
     rm /tmp/pointercal

     ;;
esac

printf "\n== Run post-calibration steps ==\n"

TSLIB_CALIBFILE="$POINTERCAL_PATH"
export TSLIB_CALIBFILE

# Now convert the result to a udev rule for weston
"$POINTERCAL_TO_UDEVRULE" --device="$TSLIB_TSDEVICE"

printf "\n== Finished touchscreen calibration ==\n"
printf "\nInfo: Please reboot the system to enable the new calibration across all services\n\n"

exit 0
