#!/bin/sh

if [ -e /dev/input/touchscreen0 ]; then
    TSLIB_TSDEVICE=/dev/input/touchscreen0

    export TSLIB_TSDEVICE
fi

if [ -r /etc/shared/pointercal ]; then
    TSLIB_CALIBFILE=/etc/shared/pointercal

    export TSLIB_CALIBFILE
fi

if [ -r /etc/shared/ts.conf ]; then
    TSLIB_CONFFILE=/etc/shared/ts.conf

    export TSLIB_CONFFILE
fi

