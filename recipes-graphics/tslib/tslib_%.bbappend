FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://0001-Ignore-unkown-modules-in-ts.conf-print-warning-but-d.patch \
    file://profile_d_ts_calibrate.sh \
    file://ts_calibrate_extended.sh \
"

do_install:append() {
    install -d ${D}${sysconfdir}/profile.d/available
    install -m 0755 ${WORKDIR}/profile_d_ts_calibrate.sh ${D}${sysconfdir}/profile.d/available/ts_calibrate.sh
    ln -s available/ts_calibrate.sh ${D}${sysconfdir}/profile.d/ts_calibrate.sh

    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/ts_calibrate_extended.sh ${D}${bindir}
    ln -s ts_calibrate_extended.sh ${D}${bindir}/ts_calibrate_extended
}

FILES:tslib-calibrate += " \
    ${bindir}/ts_calibrate_extended \
    ${bindir}/ts_calibrate_extended.sh \
    ${sysconfdir}/profile.d/available/ts_calibrate.sh \
    ${sysconfdir}/profile.d/ts_calibrate.sh \
"
