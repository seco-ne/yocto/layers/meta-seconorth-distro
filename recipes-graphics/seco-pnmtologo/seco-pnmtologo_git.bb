SUMMARY = "SECO variant of the pnmtologo tool"
DESCRIPTION = "\
    The SECO Northern Europe varaint of the in-kernel pnmtologo tool adds the \
    binary output format. The binary format can be used to supply a boot logo \
    to the kernel via command line"
AUTHOR = "Clemens Terasa <clemens.terasa@garz-fricke.com>"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/gfpnmtologo"
SECTION = "graphics"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e6a75371ba4d16749254a51215d13f97"

PV = "1+git${SRCPV}"
SRC_URI = "git://git.seco.com/seco-ne/tools/gfpnmtologo.git;protocol=https;branch=master"
SRCREV = "0b3ede555fa126eac2fc091ba182c3d88ce87501"

S = "${WORKDIR}/git"

inherit autotools

BBCLASSEXTEND += "native nativesdk"
