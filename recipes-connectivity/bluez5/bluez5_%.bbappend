FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://0001-l2ping-Add-daemonized-version.patch \
    file://0002-Makefile.tools-add-l2pingd-5.64.patch \
"

SRC_URI:remove:genio = " file://0002-Makefile.tools-add-l2pingd-5.64.patch "
SRC_URI:append:genio = " file://0002-Makefile.tools-add-l2pingd-5.65.patch "
