SUMMARY = "RTL88X2BU Driver"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit module

SRC_URI = "git://github.com/cilynx/rtl88x2bu.git;protocol=https;branch=5.8.7.1_35809.20191129_COEX20191120-7777 \
           file://0001-Modify-Makefile-for-compatibility.patch \
           "

SRCREV = "620b1a12c8822ee7d340465fbdc9d5150b193189"

S = "${WORKDIR}/git"


EXTRA_OEMAKE:append = " KSRC=${STAGING_KERNEL_DIR}"
