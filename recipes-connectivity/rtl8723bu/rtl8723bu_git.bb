SUMMARY = "RTL82723BU Driver"
DESCRIPTION = "Driver for wifi devices based on the realtek RTL82723BU chip."
HOMEPAGE = "https://github.com/lwfinger/rtl8723bu"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit module

RDEPENDS:${PN} += "linux-firmware-rtl8723"

SRC_URI = "git://github.com/lwfinger/rtl8723bu.git;protocol=https;branch=master \
           file://0001-disable-concurrent-mode-and-change-architecture.patch \
           "

SRCREV = "af3a408d6399655b0db23c2c8720436ca725ca47"

S = "${WORKDIR}/git"

EXTRA_OEMAKE:append = " KSRC=${STAGING_KERNEL_DIR}"
