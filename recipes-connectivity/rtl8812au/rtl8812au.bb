SUMMARY = "Realtek 802.11n WLAN Adapter Linux driver"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b234ee4d69f5fce4486a80fdaf4a4263"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit module

SRC_URI = " \
    git://github.com/aircrack-ng/rtl8812au.git;protocol=https;branch=v5.6.4.2 \
    file://0001-Use-modules_install-as-wanted-by-yocto.patch \
"

SRCREV = "37e27f9165300c89607144b646545fac576ec510"

S = "${WORKDIR}/git"

EXTRA_OEMAKE:append = " KSRC=${STAGING_KERNEL_DIR}"
