FILESEXTRAPATHS:prepend := "${THISDIR}/openssh:"

SRC_URI += " \
    file://0001-Add-rw-remount-of-shared-partition.patch;patchdir=${WORKDIR} \
"

do_install:append() {
    # 1. Move the keys to shared partition, so they don't need to be generated each boot
    # 2. Change sftp-server to build-in version of openssh
    sed -i -e 's|#HostKey /etc/ssh/|HostKey /etc/shared/ssh/|' \
        ${D}${sysconfdir}/ssh/sshd_config
    sed -i -e 's|HostKey /var/run/ssh/|HostKey /etc/shared/ssh/|' \
        ${D}${sysconfdir}/ssh/sshd_config_readonly

    # The config partition should be available before the service is started.
    sed -i -e 's|RequiresMountsFor=.*|& /etc/shared|' \
        ${D}${systemd_system_unitdir}/sshdgenkeys.service
}
