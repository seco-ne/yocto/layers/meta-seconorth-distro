FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

PACKAGES =+ "${PN}-cfg"
ALLOW_EMPTY:${PN}-cfg = "1"
RDEPENDS:${PN}-cfg += "${PN}"

do_install:append() {
    install -d ${D}${sysconfdir}
    ln -s /etc/shared/fw_env.config ${D}${sysconfdir}/fw_env.config
}

# Check if u-boot already has installed the /etc/shared/fw_env.config
# Otherwise try to generate it
pkg_postinst_ontarget:${PN}-cfg() {
    [ -r /etc/shared/fw_env.config ] && exit 0 # File already exists

    partitions="/dev/mmcblk0 /dev/mmcblk0boot1"
    offsets="0x0000 0x400000 0x800000"
    sizes="0x1000 0x2000 0x4000 0x8000"

    conffile=$(mktemp)

    for p in $partitions;do
        for o in $offsets;do
            for s in $sizes;do
                # Write a temporary config file
                echo "$p    $o       $s" > "$conffile"
                if fw_printenv -f none -c "$conffile" >/dev/null 2>&1;
                then
                    mv $conffile /etc/shared/fw_env.config
                    exit 0
                fi
            done
        done
    done
}
