DESCRIPTION = "A minimal Image for the SECO Northern Europe boards. \
It contains only the minimal base system."

require seconorth-image.inc

IMAGE_FEATURES += " \
    ssh-server-openssh \
"

CORE_IMAGE_EXTRA_INSTALL += " \
    packagegroup-seconorth-minimal \
"

# @TODO: Should we rename the user to seco?
EXTRA_USERS_PARAMS = " \
    useradd guf; \
"

