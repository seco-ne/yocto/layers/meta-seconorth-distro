SUMMARY ??= "SECO Northern Europe Flash 'N' Go ${PN}"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit core-image features_check on_bootloader

# Debug tweak needed for root without passwd
IMAGE_FEATURES = "ssh-server-openssh empty-root-password allow-empty-password allow-root-login"

# Flash-N-Go-System doesn't use systemd (because of its size).
# Make sure we are using sysvinit instead.
REQUIRED_DISTRO_FEATURES = "sysvinit"

EXTRA_IMAGE_FSTYPES ??= ""

# MediaTek uses a custom image conversion command (meta-mediatek-bsp/classes
# /image_type_img.bbclass) to create Android-like sparse images. The Genio
# tooling is tailored towards this image type which is why the "normal" gzip
# type doesn't work here.
EXTRA_IMAGE_FSTYPES:seco-mtk = "wic.img"

IMAGE_FSTYPES = "fngsystem ${EXTRA_IMAGE_FSTYPES}"

IMAGE_LINGUAS = "en-us"

REMOVED_FOR_NOW = " \
    automount-fngsystem \
    busybox-ftpd \
    busybox-telnetd \
    cgetty \
    cmd-line-env \
    glibc-ld-linux-compatibility \
    packagegroup-seconorth-graphics-fw \
    packagegroup-seconorth-kernel-modules \
    packagegroup-seconorth-wireless-fw \
    util-linux-2.25-sfdisk \
    xxd \
"

# Removed because of gplv3
#    nano \
#

IMAGE_INSTALL_FNG = " \
    bootselect \
    busybox \
    busybox-ifplugd \
    curl \
    devmem2 \
    dhcpcd \
    dosfstools \
    dtc \
    e2fsprogs-e2fsck \
    e2fsprogs-mke2fs \
    e2fsprogs-resize2fs \
    e2fsprogs-tune2fs \
    ethtool \
    evtest \
    fbv \
    fnglogo \
    gfeeprom \
    gfi2c \
    gptfdisk \
    iperf3 \
    jq \
    less \
    libftdi \
    libgpiod-tools \
    lsb-release \
    mmc-utils \
    mtd-utils \
    mtools \
    ntpdate \
    openssl \
    os-release \
    packagegroup-seconorth-productiontests \
    parted \
    seco-eeprom-manager \
    sfdisk-wrapper \
    touchcal-conv \
    tslib-calibrate \
    tslib-tests \
    util-linux \
    xconfig \
    ${@ on_bootloader_uboot(d,' \
        libubootenv \
        libubootenv-bin \
    ','')} \
"

IMAGE_INSTALL_FNG:append:mx8m-generic-bsp = " \
    kernel-module-smsc95xx \
"

# @TODO: With an included Kernel, the initramfs is too big.
# Since the Kernel is loaded separately anyways, we remove it
# from the ramfs for now.
PACKAGE_EXCLUDE = " \
    kernel-image-* \
"

PACKAGE_INSTALL += " \
    ${IMAGE_INSTALL_FNG} \
"

PACKAGE_DIR = "${WORKDIR}/${BP}-package"

# The vendor and machine layers add artefacts to the
# IMAGE_BOOT_FILES that should not be included in the
# ramfs. The workaround is to create a new variable and
# remove the unwanted artefacts.
# The FNG_BOOT_FILES may be pre-filled by the machine
# layers, hence the append operation instead of an
# assignment.
FNG_BOOT_FILES += "${IMAGE_BOOT_FILES}"
FNG_BOOT_FILES:remove = "${KERNEL_IMAGETYPES} fngsystem-image.cpio.gz"

# The WiC image creation depends on the fngsystem image
do_image_wic[depends] += "${PN}:do_image_fngsystem"

do_image_fngsystem:mx6[depends] += "uuu-scripts:do_deploy"
do_image_fngsystem:mx8[depends] += "uuu-scripts:do_deploy"
