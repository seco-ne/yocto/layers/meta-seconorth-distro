DESCRIPTION = "The SECO Northern Europe Yocto Image with additional Features. \
It contains the base system plus Wayland, Qt5 and Examples."

require seconorth-image.bb

IMAGE_INSTALL_MULTIMEDIA += " \
    packagegroup-seconorth-multimedia-extra \
    packagegroup-seconorth-multimedia-tests \
"

IMAGE_INSTALL_QT += " \
    packagegroup-seconorth-qt5-examples \
"

