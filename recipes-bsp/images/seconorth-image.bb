DESCRIPTION = "The SECO Northern Europe Standard Yocto Image. \
It contains the base system plus Wayland and Qt5."

require seconorth-image.inc

inherit populate_sdk_qt5 features_check

REQUIRED_DISTRO_FEATURES = "wayland"

IMAGE_FEATURES += " \
    hwcodecs \
    package-management \
    splash \
    ssh-server-openssh \
    tools-debug \
"

IMAGE_INSTALL_GRAPHICS += " \
    packagegroup-seconorth-graphics-fonts \
    packagegroup-seconorth-graphics-wayland \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'packagegroup-seconorth-graphics-x11', '', d)} \
"

IMAGE_INSTALL_SECONORTH += " \
    seco-show-demo \
    packagegroup-seconorth-debug \
    packagegroup-seconorth-small \
"

IMAGE_INSTALL_MULTIMEDIA += " \
    packagegroup-seconorth-multimedia \
"

IMAGE_INSTALL_QT += " \
    packagegroup-seconorth-qt5 \
    packagegroup-seconorth-qt5-tests \
    packagegroup-seconorth-qt5-tools \
    packagegroup-seconorth-qt5-translations \
"

IMAGE_INSTALL_PYTHON += " \
    packagegroup-seconorth-python \
"

IMAGE_INSTALL_TEST += " \
    packagegroup-seconorth-tests \
"

IMAGE_INSTALL_CONNECTIVITY += " \
    packagegroup-seconorth-connectivity \
"

IMAGE_INSTALL_BENCHMARK += " \
    packagegroup-seconorth-benchmark \
"

IMAGE_INSTALL_IOT += " \
    packagegroup-seconorth-iot \
"

CORE_IMAGE_EXTRA_INSTALL += " \
    ${IMAGE_INSTALL_BENCHMARK} \
    ${IMAGE_INSTALL_CONNECTIVITY} \
    ${IMAGE_INSTALL_GRAPHICS} \
    ${IMAGE_INSTALL_IOT} \
    ${IMAGE_INSTALL_MULTIMEDIA} \
    ${IMAGE_INSTALL_PYTHON} \
    ${IMAGE_INSTALL_QT} \
    ${IMAGE_INSTALL_SECONORTH} \
    ${IMAGE_INSTALL_TEST} \
"

# @TODO: Should we rename the user to seco?
EXTRA_USERS_PARAMS = " \
    useradd guf; \
"

