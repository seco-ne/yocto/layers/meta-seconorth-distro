# Common Definitions for SECO Northern Europe Images
SUMMARY = "SECO Northern Europe Yocto Image"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

IMAGE_BOOT_FILES = "${KERNEL_IMAGETYPE}"

inherit core-image extrausers image-buildinfo

