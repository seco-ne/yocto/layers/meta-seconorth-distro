DESCRIPTION = "The SECO Northern Europe Yocto Image with reduced Features. \
It contains the base system and Wayland but doesn't come with Qt5."

require seconorth-image.inc

inherit features_check

REQUIRED_DISTRO_FEATURES = "wayland"

IMAGE_FEATURES += " \
    hwcodecs \
    package-management \
    splash \
    ssh-server-openssh \
    tools-debug \
"

IMAGE_INSTALL_GRAPHICS += " \
"

IMAGE_INSTALL_SECONORTH += " \
    packagegroup-seconorth-small \
"

IMAGE_INSTALL_MULTIMEDIA += " \
    packagegroup-seconorth-multimedia \
"

IMAGE_INSTALL_QT += " \
"

IMAGE_INSTALL_TEST += " \
  packagegroup-seconorth-tests-small \
"

IMAGE_INSTALL_CONNECTIVITY += " \
    packagegroup-seconorth-connectivity \
"

CORE_IMAGE_EXTRA_INSTALL += " \
    ${IMAGE_INSTALL_CONNECTIVITY} \
    ${IMAGE_INSTALL_GRAPHICS} \
    ${IMAGE_INSTALL_SECONORTH} \
    ${IMAGE_INSTALL_MULTIMEDIA} \
    ${IMAGE_INSTALL_QT} \
    ${IMAGE_INSTALL_TEST} \
"

# @TODO: Should we rename the user to seco?
EXTRA_USERS_PARAMS = " \
    useradd guf; \
"

