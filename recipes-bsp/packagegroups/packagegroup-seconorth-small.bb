DESCRIPTION = "SECO Northern Europe Small Image Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup on_bootloader

PACKAGES = " \
    ${PN} \
"

RDEPENDS:${PN} = " \
    busybox \
    can-utils \
    ethtool \
    fuse-exfat \
    gfeeprom \
    less \
    libgpiod \
    libgpiod-tools \
    libmdb \
    libsocketcan \
    lmsensors \
    lsb-release \
    mmc-utils \
    ntfs-3g \
    packagegroup-seconorth-minimal \
    packagegroup-seconorth-networking \
    pciutils \
    touchcal-conv \
    tslib-calibrate \
    tslib-tests \
    tslib-uinput \
    tzdata \
    udev-extraconf \
    util-linux \
    vim \
    ${@on_bootloader_uboot(d,' \
        libubootenv \
        libubootenv-bin \
    ','')} \
    watchdog \
"

RRECOMMENDS:${PN} = " \
    ${@on_bootloader_uboot(d,' \
        libubootenv-cfg \
    ','')} \
"
