DESCRIPTION = "SECO Northern Europe Python 3 Support Packagegroup"

inherit packagegroup

PACKAGES = " \
    ${PN} \
"

RDEPENDS:${PN} = " \
    python3 \
    python3-pip \
    python3-pyserial \
"

