DESCRIPTION = "SECO Northern Europe Minimal Image Packagegroup"

inherit packagegroup

PACKAGES = " \
    ${PN} \
"

RDEPENDS:${PN} = " \
    dtc \
    nano \
    packagegroup-seconorth-networking-minimal \
    xconfig \
    logrotate \
"
