DESCRIPTION = "SECO Northern Europe Network Tools Packagegroup"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-minimal \
"

RDEPENDS:${PN} = " \
    ${PN}-minimal \
    iptables \
    modemmanager \
    networkmanager \
    networkmanager-bash-completion \
    networkmanager-nmtui \
    nginx \
    ppp \
    wireguard-tools \
"

RDEPENDS_${PN}-minimal = " \
    openssh-sftp-server \
"
