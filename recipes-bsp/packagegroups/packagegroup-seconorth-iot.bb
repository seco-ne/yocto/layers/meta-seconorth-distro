DESCRIPTION = "SECO IoT Support Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
"

RDEPENDS:${PN} = " \
    edgehog-device-runtime \
"

