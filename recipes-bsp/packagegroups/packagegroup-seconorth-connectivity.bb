DESCRIPTION = "SECO Northern Europe Connectivity Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
"

RDEPENDS:${PN} = " \
    bluez5 \
    curl \
    libmodbus \
    lldpd \
    nfs-utils-client \
    rtl8812au \
    rtl8723bu \
"

RDEPENDS:${PN}:append:seco-mx6 = " \
    rtl88x2bu  \
"