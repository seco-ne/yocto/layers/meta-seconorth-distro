DESCRIPTION = "SECO Northern Europe Test Packagegroup"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-extra \
    ${PN}-small \
"

RDEPENDS:${PN}-small = " \
    ${@bb.utils.contains_any('DISTRO_FEATURES', 'opengl dispmanx', 'glmark2', '', d)} \
    bbb \
    beep \
    drm-framebuffer \
    evtest \
    fbgrab \
    gf-productiontests \
    gfi2c \
    i2c-tools \
    spidev-test \
    iperf3 \
    libdrm-tests \
    libmdb-tests \
    memtester \
    packagegroup-seconorth-productiontests \
    stress \
"

RDEPENDS:${PN}-extra = " \
    hdparm \
    iozone3 \
"

# Note: Don't add Qt tests to this packagegroup. Qt tests trigger a full
# Qt build, even if Qt isn't required in the image or distro. Instead,
# add them to the tests package in the Qt packagegroup.

RDEPENDS:${PN} = " \
    ${PN}-small \
    emc-test-suite \
    packagegroup-seconorth-multimedia-tests \
    phytool \
"
