DESCRIPTION = "SECO Northern Europe Qt5 Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-examples \
    ${PN}-tests \
    ${PN}-tools \
    ${PN}-translations \
"

RDEPENDS:${PN} = " \
    qt3d \
    qtbase \
    qtbase-fingerpaint \
    qtcharts \
    qtconnectivity \
    qtdatavis3d \
    qtdeclarative \
    qtgamepad \
    qtgraphicaleffects \
    qtimageformats \
    qtknx \
    qtlocation \
    qtmqtt \
    qtmultimedia \
    qtnetworkauth \
    qtquickcontrols \
    qtquickcontrols2 \
    qtremoteobjects \
    qtscript \
    qtscxml \
    qtsensors \
    qtserialbus \
    qtserialport \
    qtsmarthome \
    qtsvg \
    qttools \
    qtvirtualkeyboard \
    qtwayland \
    qtwebchannel \
    qtwebglplugin \
    qtwebsockets \
    qtxmlpatterns \
    qwt-qt5 \
    qt-kiosk-browser \
"

RDEPENDS:${PN}-tests = " \
    cinematicexperience \
    cinematicexperience-tools \
    qt-dpi-test-tools \
    qt-mouse-test-tools \
    qt-playsound \
    qt-playsound-tools \
    qt5-opengles2-test \
"

RDEPENDS:${PN}-tools = " \
    qtdeclarative-tools \
    qttools-tools \
"

RDEPENDS:${PN}-translations = " \
    qttranslations-qtbase \
    qttranslations-qtconnectivity \
    qttranslations-qtdeclarative \
    qttranslations-qtlocation \
    qttranslations-qtmultimedia \
    qttranslations-qtquickcontrols \
    qttranslations-qtserialport \
    qttranslations-qtwebsockets \
    qttranslations-qtxmlpatterns \
"

RRECOMMENDS:${PN}-examples = " \
    qmlbench \
    qtbase-examples \
    qtcharts-examples \
    qtdatavis3d \
    qtdatavis3d-examples \
    qtknx-examples \
    qtmqtt-examples \
    qtmultimedia-examples \
    qtnetworkauth-examples \
    qtremoteobjects-examples \
    qtscxml-examples \
    qtsensors-examples \
    qtserialbus-examples \
    qtserialport-examples \
    qtsvg-examples \
    qtvirtualkeyboard-examples \
    qtwebchannel-examples \
    qtwebsockets-examples \
    qtxmlpatterns-examples \
"

