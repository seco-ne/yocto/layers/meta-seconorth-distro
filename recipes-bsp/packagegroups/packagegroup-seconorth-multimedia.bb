DESCRIPTION = "SECO Northern Europe Multimedia Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-tests \
    ${PN}-extra \
"

RDEPENDS:${PN} = " \
    alsa-utils \
    gstreamer1.0 \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
"

RDEPENDS:${PN}:append:mx6ull = " \
    init-mqs-audio \
"

RDEPENDS:${PN}-tests = " \
    bbb \
"

RDEPENDS:${PN}-tests:append:mx8m = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'ptest', 'libimxdmabuffer-ptest', '', d)} \
    libimxdmabuffer \
"

# The imxvpuapi library isn't compatible
# with the mx8mp.
RDEPENDS:${PN}-tests:append:mx8mm = " \
    libimxvpuapi2 \
"

RDEPENDS:${PN}-extra = ""

