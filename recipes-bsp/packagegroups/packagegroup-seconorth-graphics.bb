DESCRIPTION = "SECO Northern Europe Graphics Support Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-fonts \
    ${PN}-wayland \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', '${PN}-x11', '', d)} \
"

RDEPENDS:${PN} = " \
"

RDEPENDS:${PN}-fonts = " \
    fontconfig \
    freetype \
    ttf-dejavu-common \
    ttf-dejavu-mathtexgyre \
    ttf-dejavu-sans \
    ttf-dejavu-sans-condensed \
    ttf-dejavu-sans-mono \
    ttf-dejavu-serif \
    ttf-dejavu-serif-condensed \
"

RDEPENDS:${PN}-wayland = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11 wayland', 'weston-xwayland xwayland', '', d)} \
    qt-multi-screen-compositor \
    weston \
    weston-examples \
    weston-init \
    weston-rotation \
"

RDEPENDS:${PN}-x11 = " \
    xclock \
    xhost \
    xeyes \
    xrandr \
"
