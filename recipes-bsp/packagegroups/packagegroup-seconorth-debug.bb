DESCRIPTION = "SECO Northern Europe Debugging Tools Packagegroup"

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-ltp \
"

RDEPENDS:${PN} = " \
    blktrace \
    bonnie++ \
    ddrescue \
    dhrystone \
    fio \
    glibc-utils \
    hdparm \
    iftop \
    iozone3 \
    iperf3 \
    ldd \
    net-tools \
    netcat \
    perf \
    powertop \
    socat \
    tcpdump \
    valgrind \
    systemd-analyze \
"

RDEPENDS:${PN}-ltp = " \
    ltp \
"
