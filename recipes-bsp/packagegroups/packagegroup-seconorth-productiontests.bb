DESCRIPTION = "SECO Northern Europe Packagegroup for Productiontests"

# @NOTE: The production tests must remain in a separate packagegroup:
# When moving them to the tests packagegroup, all other tests are built
# too (as part of the Yocto build process). Since the production tests
# are included in Flash-N-Go System, the build time of the fngsystem-image
# is heavily increased, if the production tests are moved to the tests
# packagegroup.

inherit packagegroup

PACKAGES = " \
    ${PN} \
"

# @NOTE: The FT1/FT2 tests/tools should also be part of Flash-N-Go
RDEPENDS:${PN} = " \
    alsa-utils-speakertest \
    beep \
    bluez5 \
    gf-productiontests \
    iperf3 \
    iw \
"
