DESCRIPTION = "SE05X AES key example"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/se05x-aes-key"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit cmake

SRC_URI = "git://git.seco.com/seco-ne/tools/secure-element-examples.git;protocol=https;branch=main;nobranch=1"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

DEPENDS = "libptmwmini"
RDEPENDS:${PN} = "libptmwmini"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${B}/se05x-aes-key ${D}${bindir}
}

FILES:${PN} += "${bindir}/se05x-aes-key"
