DESCRIPTION = "NXP Plug & Trust Middleware Mini Package"
HOMEPAGE = "https://github.com/NXP/plug-and-trust"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://Apache_2_0.txt;md5=3b83ef96387f14655fc854ddc3c6bd57"

inherit cmake
DEPENDS = "openssl"
RDEPENDS:${PN} = "libcrypto libssl"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRCREV = "cc00ff155507f38b241aa8c8f700b8f2da9682f2"
SRC_URI = " \
    git://github.com/NXP/plug-and-trust.git;branch=master;protocol=https \
    file://0001-Configure-SE-applet-for-SE050.patch \
    file://0002-Add-se05x-reset-functions-for-imx-devices.patch \
    file://0003-Build-as-shared-library.patch \
    file://0004-Enable-Platform-SCP-authentication-with-default-keys.patch \
"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

FILES:${PN} += "${base_libdir}/*.so.*"
FILES:${PN}-dev += "${base_libdir}/*.so ${includedir}/*"
