SUMMARY = "Qt multi-screen Compositor"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/qt-multi-screen-compositor"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD-3-Clause;md5=550794465ba0ec5312d6919e203a55f9"

DEPENDS = "qtbase qtquickcontrols"
RDEPENDS:${PN} += "qtwayland"

SRCREV = "${AUTOREV}"

SRC_URI = " \
    git://git.seco.com/seco-ne/tools/qt-multi-screen-compositor.git;protocol=https;branch=master;nobranch=1 \
    file://qt-autologin \
    file://qt-multi-screen-compositor.service \
"

# Starting the qt-compositor via a systemd socket doesn't work out of the box.
# The systemd socket unit opens the socket and waits for requests from applications
# on this socket. When an application connects, the unit starts the service itself.
# The daemon (in this case the qt-compositor), then needs to re-use the socket from
# the socket unit. Right now, our qt-multi-screen-compositor implementation can't
# re-use the socket and opens a new socket instead. This behavior blocks the starting
# Wayland application (e.g. fingerpaint).
# Until our compositor implementation handles the requests properly,
# we stick to the old way of manually starting the service unit.

#SRC_URI += " \
#    file://qt-multi-screen-compositor.socket \
#"

SRC_URI:append:mx8m = " \
    file://kms.conf \
"

inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

inherit autotools
inherit systemd

require recipes-qt/qt5/qt5.inc

FILESEXTRAPATHS:prepend := "${THISDIR}:"

PACKAGES:remove = "${PN}-tools"

QMAKE_PROFILES = "${S}/compositor.pro"

do_install () {
    # Application
    install -d ${D}${bindir}
    install -m 0755 multiscreen-compositor/multiscreen-compositor ${D}${bindir}
    install -m 0755 compositor-message/compositor-message ${D}${bindir}

    if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
        # Systemd Unit
        install -d ${D}${systemd_system_unitdir}/
        install -D -p -m 0644 ${WORKDIR}/qt-multi-screen-compositor.service ${D}${systemd_system_unitdir}/qt-multi-screen-compositor.service
        #install -D -p -m 0644 ${WORKDIR}/qt-multi-screen-compositor.socket ${D}${systemd_system_unitdir}/qt-multi-screen-compositor.socket
    fi

    if ${@bb.utils.contains('DISTRO_FEATURES','pam','true','false',d)}; then
        # Autologin for Service Unit
        install -D -p -m 0644 ${WORKDIR}/qt-autologin ${D}${sysconfdir}/pam.d/qt-autologin
    fi
}

do_install:append:mx8m () {
    install -d ${D}${sysconfdir}/
    install -m 0664 ${WORKDIR}/kms.conf ${D}${sysconfdir}/kms.conf
}

FILES:${PN} += " \
    ${sysconfdir}/pam.d/ \
    ${systemd_system_unitdir}/qt-multi-screen-compositor.service \
"

#FILES:${PN} += " \
#    ${systemd_system_unitdir}/qt-multi-screen-compositor.socket \
#"

SYSTEMD_SERVICE:${PN} = " \
    qt-multi-screen-compositor.service \
"

#SYSTEMD_SERVICE:${PN} += " \
#    qt-multi-screen-compositor.socket \
#"

inherit useradd

USERADD_PACKAGES = "${PN}"
USERADD_PARAM:${PN} = "--home /home/qt --shell /bin/sh --user-group -G video,input,audio qt"
GROUPADD_PARAM:${PN} = "--system wayland"
