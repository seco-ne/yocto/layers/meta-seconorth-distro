SUMMARY = "GammaRay Qt introspection probe"
HOMEPAGE = "https://www.kdab.com/gammaray"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSES/GPL-2.0-or-later.txt;md5=3d26203303a722dedc6bf909d95ba815"

inherit cmake_qt5

SRC_URI = "git://github.com/KDAB/GammaRay;branch=master;protocol=https"

SRCREV = "08614b34fe6cc36f902ef698d2cb6746404ac704"
PV = "2.7.0+git${SRCPV}"

DEPENDS = "qtdeclarative"

S = "${WORKDIR}/git"

EXTRA_OECMAKE += " -DGAMMARAY_BUILD_UI=OFF"

FILES:${PN}-dev += " \
    /usr/lib/cmake/* \
    /usr/mkspecs/modules/* \
"
FILES:${PN}-dbg += " \
    /usr/lib/.debug/* \
    /usr/lib/gammaray/*/*/.debug \
    /usr/lib/gammaray/*/*/styles/.debug \
"

PACKAGES += "${PN}-zsh"
FILES:${PN}-zsh = " \
    /usr/share/zsh/* \
"
