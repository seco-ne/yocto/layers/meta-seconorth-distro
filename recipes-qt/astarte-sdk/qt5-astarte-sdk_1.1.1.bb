SUMMARY = "Astarte Qt5 SDK"
HOMEPAGE = "https://github.com/astarte-platform/astarte-device-sdk-qt5"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS += "qtbase mosquitto openssl"
RDEPENDS:${PN} += "libmosquittopp1"

SRC_BRANCH = "master"
SRC_URI = " \
    git://github.com/astarte-platform/astarte-device-sdk-qt5.git;protocol=https;branch=${SRC_BRANCH};nobranch=1 \
"

SRCREV = "a652f06cb76f623250a5fbc504b1b9bbba8b2b5b"

S = "${WORKDIR}/git"

inherit cmake_qt5

FILES:${PN} = " \
    ${bindir}/astarte-validate-interface \
    ${datadir}/hyperdrive \
    ${libdir}/libAstarteDeviceSDKQt5* \
"
