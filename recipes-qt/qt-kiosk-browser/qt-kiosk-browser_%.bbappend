FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append = " file://qt-kiosk-browser.service"

inherit systemd

do_install:append() {
    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/qt-kiosk-browser.service ${D}${systemd_system_unitdir}
}

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "qt-kiosk-browser.service"
SYSTEMD_AUTO_ENABLE = "disable"
