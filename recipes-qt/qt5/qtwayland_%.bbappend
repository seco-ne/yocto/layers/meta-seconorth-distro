FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://0001-qtbug-128350-compositor_destroy_qopengltexture_with_sharedmemorybuffer.patch"

# etnaviv mesa does not have glx
PACKAGECONFIG:remove = "xcomposite-glx"
