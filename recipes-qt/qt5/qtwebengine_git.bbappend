# The qtwebengine builds memory consumption is dependent on the number of ninja
# tasks. This in turn depends on the PARALLEL_MAKE variable. In our build
# environment with currently 24 cores it exhausts all the available memory.
# Even worse on the CI machine that does bitbake jobs in parallel.
PARALLEL_MAKE = "-j 8"
