FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://0001-QStylesheetStyle-fix-push-button-label-alignment.patch"

# Use link in openssl since we are using OpenSSL 3.0.x
OPENSSL_LINKING_MODE = "-linked"

PACKAGECONFIG_DISTRO:append = " ${@bb.utils.contains('DISTRO_FEATURES', 'opengl', 'gbm gles2 kms eglfs', 'no-opengl', d)} "
PACKAGECONFIG_DISTRO:remove = " ${@bb.utils.contains('MACHINE', 'seco-mx6-fsl', 'gbm', '', d)} "
PACKAGECONFIG_DISTRO:append = " linuxfb "
PACKAGECONFIG_DISTRO:append = " gif "
PACKAGECONFIG_DISTRO:append = " sql-sqlite2 sql-sqlite sql-mysql sql-psql "
PACKAGECONFIG_DISTRO:append = " examples "
PACKAGECONFIG_DISTRO:append = " icu "
PACKAGECONFIG_FONTS:append = " fontconfig "

do_install:append() {

    # We don't install the examples by default but fingerpaint is widely used for testing
    install -d ${D}/${bindir}
    install -m 0755 ${B}/examples/widgets/touch/fingerpaint/fingerpaint ${D}/${bindir}/fingerpaint

}

PACKAGES =+ "${PN}-fingerpaint"
FILES:${PN}-fingerpaint += "   ${bindir}/fingerpaint "
