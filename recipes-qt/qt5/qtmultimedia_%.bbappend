FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:mx8m = "file://0001-Fix-kernel-warning-about-non-freed-memory.patch"

PACKAGECONFIG:append = " gstreamer"
PACKAGECONFIG:append = " examples"

# The "Replace resolved lib path with the lib name" of qtmake5_base.bbclass 
# mangles with soem cmake files. The fix in the bbappend does not help with 
# anything. Redo the copy from the build dir.
do_install:append () {
    if [ -f "${B}/lib/cmake/Qt5Multimedia/Qt5Multimedia_QSGVideoNodeFactory_EGL.cmake" ]; then
        install -m 644 ${B}/lib/cmake/Qt5Multimedia/Qt5Multimedia_QSGVideoNodeFactory_EGL.cmake  ${D}${libdir}/cmake/Qt5Multimedia/Qt5Multimedia_QSGVideoNodeFactory_EGL.cmake
    fi
    if [ -f "${B}/lib/cmake/Qt5Multimedia/Qt5Multimedia_QSGVivanteVideoNodeFactory.cmake" ]; then
        install -m 644 ${B}/lib/cmake/Qt5Multimedia/Qt5Multimedia_QSGVivanteVideoNodeFactory.cmake  ${D}${libdir}/cmake/Qt5Multimedia/Qt5Multimedia_QSGVivanteVideoNodeFactory.cmake
    fi
}
