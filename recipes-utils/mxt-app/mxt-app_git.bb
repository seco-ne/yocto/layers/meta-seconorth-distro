DESCRIPTION = "Command line utility for maXTouch devices"
SECTION = "tools"
HOMEPAGE = "https://github.com/atmel-maxtouch/mxt-app"
LICENSE = "BSD-2-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=8b6acde4490765c7b838377ac61e2d2d"

DEPENDS = "libusb1"

PV = "1.36+git${SRCPV}"

inherit autotools

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRCREV="5700074206140c501dd3e7d7ff8a1ee9de9079d5"
SRC_URI = " \
    git://github.com/atmel-maxtouch/mxt-app.git;branch=master;protocol=https \
    file://0001-handle-file_info_header-section-in-config-parser.patch \
"

S = "${WORKDIR}/git"
