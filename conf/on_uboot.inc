
def on_uboot(d, a, b):
    if d.getVar('UBOOT_CONFIG') or d.getVar('UBOOT_MACHINE'):
        return a
    else:
        return b
