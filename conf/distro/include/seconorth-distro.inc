require conf/distro/poky.conf

inherit kernel-artifact-names

SDK_VENDOR = "-seconorthsdk"
MAINTAINER = "SECO Northern Europe GmbH <support.north@seco.com>"
TARGET_VENDOR = "-seconorth"
DISTROOVERRIDES = "seconorth"
DISTRO_VERSION = "${@d.getVar('MANIFEST_VERSION') if d.getVar('MANIFEST_VERSION') else d.getVar('DISTRO_CODENAME')}"

# From poky/meta-poky/conf/local.conf.sample.extended:
INIT_MANAGER = "systemd"
VIRTUAL-RUNTIME:dev_manager ?= "udev"

# As older fngsystem versions complain about the tar format 'warning: skipping header 'x'
# we stay with the previous tar command. 
# See poky/meta/classes/image_types.bbclass f79b64e35a43458bd313dd5fa5c44966d8665b07
IMAGE_CMD:tar:forcevariable:seco-nxp = "${IMAGE_CMD_TAR} --sort=name --format=gnu --numeric-owner -cf ${IMGDEPLOYDIR}/${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.tar -C ${IMAGE_ROOTFS} . || [ $? -eq 1 ]"

WARN_QA:remove = "host-user-contaminated"

# Log information on images and packages
INHERIT += "buildhistory on_bootloader"
BUILDHISTORY_COMMIT = "1"

# DISTRO_RELEASE_ARTEFACTS is used by the CI to package the files for the release
DISTRO_RELEASE_ARTEFACTS = " \
    ${@' '.join([ d.getVar('IMAGE_LINK_NAME') + '.' + fs for fs in d.getVar('IMAGE_FSTYPES').split() ] )} \
    ${IMAGE_LINK_NAME}.manifest \
    ${IMAGE_LINK_NAME}.testdata.json \
    ${MACHINE_INSTALL_SCRIPTS} \
    ${MACHINE_RELEASE_ARTEFACTS} \
"

# On imx8m we currently don't have the complete config infrastructure
# so we need to specify the devicetree to use. For this we need the files
# additionally outside the image
DISTRO_RELEASE_ARTEFACTS:append:mx8 = " \
    ${@' '.join([ dtb.split('/')[-1] for dtb in d.getVar('KERNEL_DEVICETREE').split() ] )} \
"
DISTRO_RELEASE_ARTEFACTS:append:mx6 = " \
    ${@' '.join([ dtb.split('/')[-1] for dtb in d.getVar('KERNEL_DEVICETREE').split() ] )} \
"

DISTRO_EXTRA_RDEPENDS += " \
    ${MACHINE_INSTALL_SCRIPTS} \
    bootselect \
"

# Move home of root to most distros default location
# default is /home/root which make sense if only /home is writeable
# and the rest of the rootfs is readonly
ROOT_HOME ?= "/root"

IMAGE_CLASSES += "image_type_fngsystem"

