FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "\
    file://0001-Set-tftp-default-blocksize-to-maximum.patch \
"

PACKAGECONFIG:append = " tftp "
