# Allow 1.21.1 version to be built by default
DEFAULT_PREFERENCE = ""

SYSTEMD_AUTO_ENABLE = "disable"

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://nginx \
"

do_install:append() {
    install -d ${D}${sysconfdir}/logrotate.d
    install -p -m 644 ${WORKDIR}/nginx ${D}${sysconfdir}/logrotate.d/nginx
}

FILSE:${PN} += " ${sysconfdif}/logrotate.d/nginx "
