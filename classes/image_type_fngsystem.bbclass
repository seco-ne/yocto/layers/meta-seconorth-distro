###############################################################################
# Bundle the initramfs, the kernel, and other boot files as tar.
# The fngsystem-self-update script takes this bundle/tar as input for the
# installation/update process of Flash-N-Go-System.
#
# Additionally, creates a separate tar archive containing the UUU tool
# and all required files for flashing the Flash-N-Go-System using UUU.
#
# @INFO: Since Kirkstone, there is a new way of disabling symlinks of the
# boot artifacts (kernel, etc.):
# To disable the creation of these links, the variable KERNEL_IMAGETYPE_SYMLINK
# can be set to 0. Since we use a FAT boot partition, links in the boot folder
# of the image lead to errors during the extraction of the image and we have
# to disable them. However, the new variable also disables the creation
# of links for the devicetrees and kernel in the deploy folder.
# To solve errors during the package of Flash-N-Go-System, the copy operations
# for the boot files and kernel have been changed (see below).
###############################################################################

inherit deploy
inherit image-artifact-names

# The still active links for the devicetrees and kernel are controlled via the
# KERNEL_DTB_LINK_NAME and KERNEL_IMAGE_LINK_NAME variables in the
# kernel-artifact-names.bbclass.
inherit kernel-artifact-names

PACKAGE_UUU_DIR =  "${PACKAGE_DIR}-uuu"

do_image_fngsystem[dirs] = "${PACKAGE_DIR} ${PACKAGE_UUU_DIR} ${IMGDEPLOYDIR} ${DEPLOYDIR}"
do_image_fngsystem[cleandirs] = "${PACKAGE_DIR} ${PACKAGE_UUU_DIR} ${DEPLOYDIR}"

IMAGE_TYPEDEP:fngsystem = "${INITRAMFS_FSTYPES}"

IMAGE_CMD:fngsystem () {
    bbnote "IMGDEPLOYDIR: ${IMGDEPLOYDIR}"
    bbnote "DEPLOY_DIR_IMAGE: ${DEPLOY_DIR_IMAGE}"
    bbnote "DEPLOYDIR: ${DEPLOYDIR}"
    bbnote "PACKAGE_DIR: ${PACKAGE_DIR}"
    bbnote "IMAGE_BASENAME: ${IMAGE_BASENAME}"
    bbnote "MACHINE: ${MACHINE}"

    # Initramfs
    cp ${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.${INITRAMFS_FSTYPES} ${PACKAGE_DIR}/${IMAGE_BASENAME}.${INITRAMFS_FSTYPES}
    cp ${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.${INITRAMFS_FSTYPES} ${PACKAGE_UUU_DIR}/${IMAGE_LINK_NAME}.${INITRAMFS_FSTYPES}

    # @NOTE: Direct copies to the deploy dir are unclean and bad. Unfortunately, we have a hard dependency between
    # the ramfs file system and the WiC image creation. Until we find a better solution, we do the direct copy to
    # enable the creation of the fngsystem WiC image.
    cp ${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.${INITRAMFS_FSTYPES} ${DEPLOY_DIR_IMAGE}/${IMAGE_BASENAME}.${INITRAMFS_FSTYPES}

    # Boot files
    for boot_file in ${FNG_BOOT_FILES}; do
        bbnote "Copying boot file: ${boot_file}"

        # The boot files consist mostly of devicetrees, but can also contain additional
        # files (e.g. the boot config). We only want to alter the copy operation for
        # the devicetrees (hence the filter operation for dtb files).
        boot_file_ext="${boot_file##*.}"

        if [ "$boot_file_ext" = dtb -o "$boot_file_ext" = dtbo ]; then # Allow .dtb and .dtbo
            # The "new" links for the devicetrees are created in the kernel-devicetree.bbclass using
            # the KERNEL_DTB_LINK_NAME variable.
            dtb="$(echo "$boot_file" | cut -d '.' -f 1)"

            if [ -f "${DEPLOY_DIR_IMAGE}/${dtb}-${MACHINE}.${boot_file_ext}" ]; then
                bbnote "Found ${DEPLOY_DIR_IMAGE}/${dtb}-${MACHINE}.${boot_file_ext}"
                cp -v -L ${DEPLOY_DIR_IMAGE}/${dtb}-${MACHINE}.${boot_file_ext} ${PACKAGE_DIR}/${boot_file}
                cp -v -L ${DEPLOY_DIR_IMAGE}/${dtb}-${MACHINE}.${boot_file_ext} ${PACKAGE_UUU_DIR}/${boot_file}
                continue
            else
                bbnote "Unable to find ${DEPLOY_DIR_IMAGE}/${dtb}-${MACHINE}.${boot_file_ext}; Using fallback ..."
            fi
        fi

        cp -v -L ${DEPLOY_DIR_IMAGE}/${boot_file} ${PACKAGE_DIR}
    done

    # Kernel
    for kernel_image in ${KERNEL_IMAGETYPES}; do
        bbnote "Copying kernel: ${kernel_image}"

        # The "new" links for the kernel are created in the kernel.bbclass
        # via the KERNEL_IMAGE_LINK_NAME variable.
        cp -v -L ${DEPLOY_DIR_IMAGE}/${kernel_image}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT} ${PACKAGE_DIR}/${kernel_image}
        cp -v -L ${DEPLOY_DIR_IMAGE}/${kernel_image}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT} ${PACKAGE_UUU_DIR}/${kernel_image}-${KERNEL_IMAGE_LINK_NAME}${KERNEL_IMAGE_BIN_EXT}
    done

    # Pack everything to one tar
    tar -cvzf ${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.tgz -C ${PACKAGE_DIR} .
    (
        cd ${IMGDEPLOYDIR}
        md5sum ${IMAGE_LINK_NAME}.tgz > ${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.md5
    )

    # Preparing UUU bundle
    for script in ${UUU_SCRIPTS_SH} ${UUU_SCRIPTS_BAT} ${UUU_EXECUTABLE} ${BOOT_ARTEFACTS}; do
        bbnote "Copying ${script} to ${PACKAGE_UUU_DIR}"
        if [ -f "${DEPLOY_DIR_IMAGE}/${script}" ]; then
            cp -v -L ${DEPLOY_DIR_IMAGE}/${script} ${PACKAGE_UUU_DIR}/${script}
        else
            bbwarn "${script} does not exist and will be skipped."
        fi
    done

    # Pack UUU to one tar
    tar -cvzf ${IMGDEPLOYDIR}/uuu-${IMAGE_LINK_NAME}.tgz -C ${PACKAGE_UUU_DIR} .
    (
        cd ${IMGDEPLOYDIR}
        md5sum uuu-${IMAGE_LINK_NAME}.tgz > ${IMGDEPLOYDIR}/uuu-${IMAGE_LINK_NAME}.md5
    )

}
