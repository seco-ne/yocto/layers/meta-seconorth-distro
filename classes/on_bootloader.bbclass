def on_bootloader_uboot(d, a, b):
    if d.getVar('UBOOT_CONFIG') or d.getVar('UBOOT_MACHINE'):
        return a
    else:
        return b

EXPORT_FUNCTIONS uboot

