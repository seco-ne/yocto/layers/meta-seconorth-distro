# Inherit this class to create Wic image with SECO North default layout

IMAGE_FSTYPES += " wic.gz "
WIC_ROOTFS_DEVICE ?= "mmcblk1"
WICVARS += " WIC_ROOTFS_DEVICE "
WKS_FILE = "seconorth-default-layout.wks.in"
WIC_CREATE_EXTRA_ARGS:remove = "--no-fstab-update"
