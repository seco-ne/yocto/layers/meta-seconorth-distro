FILESEXTRAPATHS:append := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://dma_heap.rules \
    file://dma_heap_symlinks.sh \
"

do_install:append() {
    install -d ${D}${sysconfdir}/udev/rules.d/
    install -d ${D}${sysconfdir}/udev/scripts/
    install -m 0644 ${WORKDIR}/dma_heap.rules ${D}/${sysconfdir}/udev/rules.d/
    install -m 0744 ${WORKDIR}/dma_heap_symlinks.sh ${D}${sysconfdir}/udev/scripts/
}

FILES:${PN}:append = " \
    ${sysconfdir}/udev/** \
"
