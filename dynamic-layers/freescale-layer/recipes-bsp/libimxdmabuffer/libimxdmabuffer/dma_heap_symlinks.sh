#!/bin/sh
#
# If the cma size is specified in the kernel command line, the
# nodes reserved and reserved-uncached are created.
# libimxdmabuffer expects linux,cma and linux,cma-uncached

POSTFIX=$( echo "$1" | sed 's/reserved//g' )
ln -s /dev/dma_heap/"$1" /dev/dma_heap/linux,cma"$POSTFIX"
