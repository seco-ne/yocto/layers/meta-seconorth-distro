# The imx-vpuwrap package was temporarily removed because the vpu support
# is currently broken and was disabled. Since the package depends on imxvpu,
# it needed to be removed too.

RDEPENDS:${PN}:append:use-nxp-bsp = " \
    imx-codec \
    imx-gpu-apitrace \
    imx-gst1.0-plugin \
    imx-vpuwrap \
    packagegroup-fsl-gstreamer1.0-full \
"

RDEPENDS:${PN}-extra:use-nxp-bsp = " \
    imx-gpu-sdk \
"

RDEPENDS:${PN}:remove:mx6ull = " \
    imx-gpu-apitrace \
    imx-vpuwrap \
"

RDEPENDS:${PN}-extra:remove:mx6ull = " \
    imx-gpu-sdk \
"
