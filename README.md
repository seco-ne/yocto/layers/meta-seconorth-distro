# meta-seconorth-distro

See README.md in the manifest project for project information.

This layer contains everything to build the standard SECO Northern Europe distribution, containing all tools and packages generally used on SECO Northern Europe HMI devices.

The main image is the ```seconorth-image``` built using:

```bitbake seconorth-image```

In a perfect world there should be no dependencies to the meta-seconorth-machine layer and the seconorth-image should be buildable for other machines too. This, however, is not tested, verified or supported.

## License

This layer is licensed under the Apache License 2.0. See also ```LICENSE.txt``` inside the root directory.
Recipes with deviating licenses have ```LICENSE.txt``` inside the recipe directories.
