SUMMARY = "SECO Northern Europe bootselect tool to switch boot mode between FNGSystem and normal OS"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = "file://bootselect"

PR = "r0"

S = "${WORKDIR}/bootselect"

DEPENDS += " libubootenv"

CFLAGS += " -Wall -Werror -std=c99 -O2"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 bootselect ${D}${bindir}
}
