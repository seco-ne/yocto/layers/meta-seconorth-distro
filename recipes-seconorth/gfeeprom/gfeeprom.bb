SUMMARY = "Tool to support the SECO Northern Europe EEPROM format using a generic EEPROM driver like the at24"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRCREV = "${AUTOREV}"
SRC_URI = "git://git.seco.com/seco-ne/tools/gfeeprom.git;protocol=https;branch=master;nobranch=1"
# Create a <tag>-<number of commits since tag>-<hash> Version string
#inherit gitpkgv
PV = "1.0"

DEPENDS += " zlib "

S = "${WORKDIR}/git"

do_install () {
	install -d ${D}${bindir}
	install -m 0755 gfeeprom ${D}${bindir}
}
