SUMMARY = "SECO EEPROM Manager"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=639136791376fdaf0cd9f92ef1edce4a"

SRC_URI = "git://git.seco.com/edgehog/tools/seco-eeprom-manager.git;protocol=https;nobranch=1"

PV = "1.0+git${SRCPV}"
SRCREV = "70faf53313b0627506cdcb9359f175f5e4da833e"

inherit autotools

INSANE_SKIP:${PN} += "ldflags"

EXTRA_OECONF = ""

FILES:${PN} += " ${bindir}/${PN} "

S = "${WORKDIR}/git"
B = "${WORKDIR}/git"

do_install() {
    mkdir -p ${D}/${bindir}
    install -m 775 ${B}/seco-eeprom-manager ${D}/${bindir}/${PN}
}
