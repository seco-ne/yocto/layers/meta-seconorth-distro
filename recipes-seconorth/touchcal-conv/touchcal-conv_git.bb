SUMMARY = "Tool to convert different touch calibration formats into another."
HOMEPAGE = "https://git.seco.com/clea-os/tools/touchcal-conv"
DESCRIPTION = "This tool takes touch calibration in the tslib format and \
               converts them to libinput format. It also allows generation \
               of calibration data to map touch min/max to screen size."

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRCREV = "f8342fb43fcfc33359726adecc950fecc60e418e"
SRC_URI = "git://git.seco.com/clea-os/tools/seco-touchcal-conv.git;protocol=https;branch=main; \
           file://touchcal-conv-weston.sh \
           "

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

inherit autotools

do_install:append() {
    install -m 0755 ${WORKDIR}/touchcal-conv-weston.sh ${D}${bindir}/

    # Create folder to use for shared data
    install -d ${D}${sysconfdir}/shared
}
