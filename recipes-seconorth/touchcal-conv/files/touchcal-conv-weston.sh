#/bin/sh
# To be use in weston.ini as calibration_helper
# It calls touchcal-conv to write the libintput
# calibration matrix converted to /etc/shared/pointercal
LOGFILE="/var/log/touchconv.log"
exec > $LOGFILE
exec 2>&1
set -x

echo "==========================="
date
echo

/usr/bin/touchcal-conv convert-l2p \
        --touchdevice "/dev/input/$( basename "$1")" \
        --matrix "$2 $3 $4 $5 $6 $7" \
        --outputfile /etc/shared/pointercal \
        -v -v
sync
