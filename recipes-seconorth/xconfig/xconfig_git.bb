SUMMARY = "xconfig tool for modifying shared XML configuration"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = "git://git.seco.com/seco-ne/tools/xconfig.git;protocol=https;branch=dunfell;nobranch=1"
SRCREV = "${AUTOREV}"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

CFLAGS += "${@bb.utils.contains("IMAGE_FSTYPES", "ubi", \
		"-DXCONFIG_TYPE_NAND", \
		"-DXCONFIG_TYPE_FILE", d)}"

#do_compile () {
#	# You will almost certainly need to add additional arguments here
#	oe_runmake
#}

do_install () {
	install -d ${D}${bindir}
	install -m 0755 build/xconfig ${D}${bindir}
}

