SUMMARY = "SECO North MDB library"
HOMEPAGE = "https://www.seco.com"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/libmdb"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS += " libgpiod " 

SRCREV = "${AUTOREV}"
SRC_URI = "git://git.seco.com/seco-ne/tools/libmdb.git;protocol=https;branch=master;nobranch=1"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

CFLAGS += "-fPIC "

do_install() {
    oe_runmake install DEST_DIR="${D}" SBINDIR="${D}${sbindir}"
}

PACKAGES += "${PN}-tests"
FILES:${PN}-tests = "${sbindir}/*"
FILES:${PN}:remove = "${sbindir}/*"
