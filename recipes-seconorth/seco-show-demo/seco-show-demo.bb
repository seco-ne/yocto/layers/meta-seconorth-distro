SUMMARY = "SECO Trade Show Demo Application"
HOMEPAGE = "https://git.seco.com/seco-ne/tools/guf-show-demo"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "qtbase qtquickcontrols qtwebengine qtvirtualkeyboard"

SRCREV = "${AUTOREV}"
SRC_URI = " \
    file://config.ini \
    file://seco-show-demo.service \
    file://system_info.ini \
    git://git.seco.com/seco-ne/tools/seco-show-demo.git;protocol=https;branch=main;nobranch=1 \
"

# Create a <tag>-<number of commits since tag>-<hash> Version string
inherit gitpkgv
PKGV = "${GITPKGVTAG}"

S = "${WORKDIR}/git"

inherit autotools
inherit systemd

require recipes-qt/qt5/qt5.inc

FILESEXTRAPATHS:prepend := "${THISDIR}:"

# ${PN}-tools added by qt5.inc, but we do not need it
PACKAGES:remove = "${PN}-tools"

QMAKE_PROFILES = "${S}/${PN}.pro"

ASSETS_DIR = "${datadir}/${PN}/assets"

do_configure:append:mx6ull() {
    sed -i ${WORKDIR}/seco-show-demo.service -e "/Environment.*wayland/a Environment=QT_QUICK_BACKEND=software"
}

do_install () {
    # Application
    install -d ${D}${bindir}
    install -m 0755 ${PN} ${D}${bindir}

    if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
            # Systemd
            install -d ${D}${systemd_system_unitdir}/
            install -D -p -m 0644 ${WORKDIR}/seco-show-demo.service ${D}${systemd_system_unitdir}/seco-show-demo.service
    fi

    # Config
    install -d ${D}${sysconfdir}/seco-show-demo
    install -m 0644 ${WORKDIR}/config.ini ${D}${sysconfdir}/seco-show-demo/
    install -m 0644 ${WORKDIR}/system_info.ini ${D}${sysconfdir}/seco-show-demo/

    # Slides
    if [ -d "${S}/assets/slides" ]; then
        mkdir -p ${D}${ASSETS_DIR}
        cp -R ${S}/assets/slides ${D}${ASSETS_DIR}/
    fi

    # Fonts
    mkdir -p ${D}${datadir}/fonts
    cp -R ${S}/fonts/* ${D}${datadir}/fonts
}

FILES:${PN} += " \
    ${ASSETS_DIR} \
    ${datadir}/fonts \
    ${sysconfdir}/seco-show-demo \
    ${systemd_system_unitdir}/seco-show-demo.service \
"

SYSTEMD_SERVICE:${PN} = "seco-show-demo.service"

