DESCRIPTION = "Big Bug Bunny trailer test video"
SECTION = "multimedia"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://trailer_1080p_h264_mp3.avi \
"

S = "${WORKDIR}"

do_compile () {
}

do_install () {
    install -d ${D}/usr/share/bbb
    install -m 644 trailer_1080p_h264_mp3.avi ${D}/usr/share/bbb
}
