SUMMARY = "Medium Quality Sound Initialization Service"
HOMEPAGE = "https://edge.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit systemd

SRC_URI = " \
    file://init-mqs-audio.service \
"

do_install () {
    install -D -p -m 0644 ${WORKDIR}/init-mqs-audio.service ${D}${systemd_system_unitdir}/init-mqs-audio.service
}

FILES:${PN} += " \
    ${systemd_system_unitdir}/init-mqs-audio.service \
"

SYSTEMD_SERVICE:${PN} = "init-mqs-audio.service"
