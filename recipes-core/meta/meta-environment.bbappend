SOURCEFOLDER := "${THISDIR}/meta-environment"
# Skip file dependency check
SKIP_FILEDEPS:${PN} = '1'
# Add additional script to start the qt-creator in the sdk's environment
create_sdk_files:append () {
	cp ${SOURCEFOLDER}/start-qtcreator.sh  ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh

	# Replace variables
	sed -i "s/=@TARGETSYS@/=${REAL_MULTIMACH_TARGET_SYS}/g" ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
	sed -i "s/=@TARGETSYS2@/=${TARGET_SYS}/g" ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
	sed -i "s/=@SDKSYS@/=${SDK_SYS}/g" ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
	sed -i "s/=@TARGETMACHINE@/=${MACHINE_ARCH}/g" ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
	sed -i "s/=@TARGETABI@/=arm-linux-generic-elf-${SITEINFO_BITS}bit/g" ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
	sed -i "s/=@SDKVERSION@/=${SDK_VERSION}/g" ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh

	# Append packed QtProject dir to start-qtcreator.sh
	tar c -C ${SOURCEFOLDER} QtProject >> ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
	#-${REAL_MULTIMACH_TARGET_SYS}
}
do_install:append () {
    install -m 0755 -t ${D}/${SDKPATH} ${SDK_OUTPUT}/${SDKPATH}/start-qtcreator.sh
}
