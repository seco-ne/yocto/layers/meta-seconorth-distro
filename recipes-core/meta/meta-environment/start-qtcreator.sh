#!/bin/bash
#
# Creates a new qtcreator configuration based on the given sdk in a config subfolder
# Sets all Pathes for the selected SDK
# Tries to integrate existing settings from qtcreator.ini
# sources the environment setup
# starts the qtcreator
#
set -e
if [ "${VERBOSE}" = 1 ];then set -x; fi

SDK_VERSION=@SDKVERSION@
TARGET_MACHINE=@TARGETMACHINE@
TARGET_SYS=@TARGETSYS@
TARGET_SYS2=@TARGETSYS2@
TARGET_ABI=@TARGETABI@
SDK_SYS=@SDKSYS@
SETUP_SCRIPT=environment-setup-${TARGET_SYS}
QTSETTINGS_DIR_DEFAULT=$HOME/.config/QtProject

usage(){
    echo "$0"
    echo "Script to setup qtcreator to use the GUF Yocto SDK and start it with those settings"
    echo "Options:"
    echo "       --sdk <path to sdk, default is location of this script>"
    echo "       --envsetup <sdk environment setup script, default is $SETUP_SCRIPT>"
    echo "       --settingsdir <directory to store the new setup settingsdir, default is $QTSETTINGS_DIR_DEFAULT-${SDK_VERSION}-${TARGET_MACHINE}>"
    echo "       --qtcreator <select the qtcreator binary, default is autoselected>"
    echo "       --help Show this help"
    echo "       --verbose  Show executed commands"
    echo "       --  End of options to the script, further options are passed to qtcreator"
    echo ""
    if [ -d /opt/guf/ ]; then
        echo "For example call one of:"
        find  /opt/guf/ -maxdepth 1 -type d  -iname GUF-Yocto\*sdk -exec echo "$0" --sdk {} \;
    fi
    echo
}

#============== parse_args ==================
while [ $# -gt 0 ]; do
    case $1 in
        -h|-\?|--help)   # Call a "usage" function to display a synopsis, then exit.
            usage
            exit
            ;;
        -v|--verbose)
            VERBOSE=1
            set -x
            ;;
        -s|--sdk)
            if [ -n "$2" ]; then
                SDK_PATH="$2"
                shift
            else
                printf 'ERROR: "--sdk" requires a non-empty option argument.\n' >&2
                exit 1
            fi
            ;;
        -s=*|--sdk=*)
            SDK_PATH="${1#*=}"
            ;;
        --envsetup)
            if [ -n "$2" ]; then
                SETUP_SCRIPT="$2"
                shift
            else
                printf 'ERROR: "--envsetup" requires a non-empty option argument.\n' >&2
                exit 1
            fi
            ;;
        --envsetup=*)
            SETUP_SCRIPT="${1#*=}"
            ;;
        --settingsdir)
            if [ -n "$2" ]; then
                QTSETTINGS_DIR="$2"
                shift
            else
                printf 'ERROR: "--settingsdir" requires a non-empty option argument.\n' >&2
                exit 1
            fi
            ;;
        --settingsdir=*)
            QTSETTINGS_DIR="${1#*=}"
            ;;
        --qtcreator)
            if [ -n "$2" ]; then
                QTCREATOR="$2"
                shift
            else
                printf 'ERROR: "--qtcreator" requires a non-empty option argument.\n' >&2
                exit 1
            fi
            ;;
        --qtcreator=*)
            QTCREATOR="${1#*=}"
            ;;
        --)            # End of all options.
            shift
            break
            ;;
        *)
            break
            ;;
    esac
    shift
done

#======= SDK ======================================
BASEDIR="$(realpath "$(dirname $0)/")"
if [ -z "${SDK_PATH}" ];then
    SDK_PATH=${BASEDIR}
fi
if [ -z "${SDK_PATH}" ] || [ ! -d "${SDK_PATH}" ]|| [ ! -e "${SDK_PATH}/${SETUP_SCRIPT}" ]; then
    echo "Failed to find the sdk, tried: ${SDK_PATH}."
    usage
    exit 1
fi
SDK_PATH="${SDK_PATH%*/}" # Remove trailing /
SDK_PATH_ESCAPED="${SDK_PATH//\//\\\/}" # Excape / with \/
SDK_PATH_ESCAPED="${SDK_PATH_ESCAPED/\./\\\.}" # Escape . with \.

#======= qt creator binary ================================
if [ -z "${QTCREATOR}" ];then
    # Try from path
    if which qtcreator >/dev/null 2>&1; then
        QTCREATOR=$(which qtcreator)
    else
        # Search in opt for qt creator executables and take the one with the highest version number
        QTCREATOR=$(for i in $( find $( find /opt/ -maxdepth 1 -type d -name qt\* ) -type f  -name qtcreator -o -name qtcreator.sh); 
        do 
            echo "$( $i  -version 2>&1 |  grep "^Qt Creator")|$i" ; 
        done | sort --version-sort --reverse | head -n 1 | cut -d '|' -f 2 )
    fi
fi
if [ ! -x "${QTCREATOR}" ];then
    echo "Failed to find the qtcreator binary."
    if [ -n "${QTCREATOR}" ]; then
        echo "Tried ${QTCREATOR}."
    fi
    echo
    usage
fi

#======= Settings Dir ======================================
if [ -z "${QTSETTINGS_DIR}" ];then
    QTSETTINGS_DIR=${QTSETTINGS_DIR_DEFAULT}-${SDK_VERSION}-${TARGET_MACHINE}
fi

if [ ! -d "${QTSETTINGS_DIR}" ];then
    # Create a new settings dir
    # Create working copy
    mkdir -p "${QTSETTINGS_DIR}"
    #cp -r $BASEDIR/QtProject ${QTSETTINGS_DIR}/
	#tar angehängt mit tar c QtProject >> script.sh
	sed -e '1,/^#-----------QTConfig----------------------#$/d' "$0" | tar  x -C "${QTSETTINGS_DIR}/"

    # Insert the SDK Path
    find "${QTSETTINGS_DIR}/QtProject" -type f \
	    -exec sed -i -e "s/@SDKPATH@/${SDK_PATH_ESCAPED}/g" \
	    -e "s/@TARGETSYS@/${TARGET_SYS}/g" \
	    -e "s/@TARGETSYS2@/${TARGET_SYS2}/g" \
	    -e "s/@TARGETABI@/${TARGET_ABI}/g" \
	    -e "s/@SDKSYS@/${SDK_SYS}/g" {} \;
    # Insert the sysroot
    PROFILE_SDK=$(sed -n 's/.*>.*\(GUF-Yocto-[^/]*-sdk\)\/sysroots\/.*/\1/p' "${QTSETTINGS_DIR}/QtProject/qtcreator/profiles.xml" )
    # Select the profile
    sed -i -e "s|key=\"PE.Profile.Name\">GUF Yocto</value>|key=\"PE.Profile.Name\">${PROFILE_SDK}</value>|" \
        ${QTSETTINGS_DIR}/QtProject/qtcreator/profiles.xml
    
    # Try to reuse usersettings
    if [ -e $HOME/.config/QtProject/QtCreator.ini ]; then
        cp $HOME/.config/QtProject/QtCreator.ini ${QTSETTINGS_DIR}/QtProject/QtCreator.ini
        # Add SIGILL no stop option
        if grep -q "GdbStartupCommands=" ~/.config/QtProject/QtCreator.ini; then 
            sed -i 's/GdbStartupCommands=.*/GdbStartupCommands=handle SIGILL nostop/' ${QTSETTINGS_DIR}/QtProject/QtCreator.ini
        else
            sed -i '/\[DebugMode\]/a GdbStartupCommands=handle SIGILL nostop' ${QTSETTINGS_DIR}/QtProject/QtCreator.ini
        fi
    fi
fi

set +x
source ${SDK_PATH}/${SETUP_SCRIPT}
echo ${QTCREATOR} -settingspath ${QTSETTINGS_DIR} $@
${QTCREATOR} -block  -settingspath ${QTSETTINGS_DIR} $@
exit
#-----------QTConfig----------------------#
