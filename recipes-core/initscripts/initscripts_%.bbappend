

do_install:append() {

    # Replace /dev/tty0 woth dev/console so the output 
    # goes to the serial console configure in the command line
    sed -i -e "s|/dev/tty0|/dev/console|" ${D}${sysconfdir}/init.d/banner.sh
    sed -i -e "s|/dev/tty0|/dev/console|" ${D}${sysconfdir}/init.d/populate-volatile.sh

}
