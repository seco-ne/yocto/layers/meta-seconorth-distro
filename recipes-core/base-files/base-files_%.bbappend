FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://profile_d_alias.sh \
    file://profile_d_qt-compositor.sh \
    file://profile_d_qt.sh \
    file://profile_d_weston.sh \
"

do_configure:append() {
    if ${@bb.utils.contains('DISTRO_FEATURES','wayland x11','true','false',d)}; then
        printf "\nexport DISPLAY=\":0\"\n" >> ${WORKDIR}/profile_d_weston.sh
    fi
}

do_install:append() {
    install -d ${D}${sysconfdir}/profile.d
    install -d ${D}${sysconfdir}/profile.d/available

    install -m 0755 ${WORKDIR}/profile_d_alias.sh ${D}${sysconfdir}/profile.d/available/alias.sh
    install -m 0755 ${WORKDIR}/profile_d_qt-compositor.sh ${D}${sysconfdir}/profile.d/available/qt-compositor.sh
    install -m 0755 ${WORKDIR}/profile_d_qt.sh ${D}${sysconfdir}/profile.d/available/qt.sh
    install -m 0755 ${WORKDIR}/profile_d_weston.sh ${D}${sysconfdir}/profile.d/available/weston.sh

    ln -s available/alias.sh ${D}${sysconfdir}/profile.d/alias.sh
    ln -s available/qt.sh ${D}${sysconfdir}/profile.d/qt.sh
    ln -s available/weston.sh ${D}${sysconfdir}/profile.d/weston.sh
}

do_configure:append:mx6ull() {
    echo export QT_QUICK_BACKEND=software >> ${WORKDIR}/profile_d_qt.sh
}
