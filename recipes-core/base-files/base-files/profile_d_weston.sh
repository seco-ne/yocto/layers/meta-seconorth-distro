# Defaults to allow start of graphical applications from the command-line

# The weston systemd service is activated by a weston socket unit.
# This socket unit listens on /run/wayland-0 for incoming requests.
# If it receives a request, it starts the service itself.
# The service then re-uses the /run/wayland-0 socket which is why
# setting the WAYLAND_DISPLAY variable to /run/wayland-0 works.

weston_uid="1000"

if id -u weston > /dev/null 2>&1 > /dev/null; then
    weston_uid=$(id -u weston)
fi

export WAYLAND_DISPLAY=/run/wayland-0
export XDG_RUNTIME_DIR=/run/user/$weston_uid
