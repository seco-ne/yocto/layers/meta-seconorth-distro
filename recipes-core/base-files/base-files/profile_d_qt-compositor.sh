# Defaults to allow start of graphical applications from the command-line

# In contrast to weston, the qt-compositor can't re-use the
# socket from a socket unit at the moment. Because of this,
# the service has to be started manually. Since the service
# runs as the "qt" user, it provides a socket under /run/user
# which has to be populated via the WAYLAND_DISPLAY variable.
export WAYLAND_DISPLAY=/run/user/1001/wayland-0 # UID = qt
export XDG_RUNTIME_DIR=/run/user/1001
