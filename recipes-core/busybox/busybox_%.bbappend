FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:fng = " \
    file://ifplugd.cfg \
    file://ifplugd.init \
    file://ifplugd.action \
    file://fngsystem-config.cfg \
"

SRC_URI:append = " \
    file://dd-ibsobs.cfg \
"

# udhcpc is running in foreground and delays the startup
# Make it go into background earlier
do_prepare_config:append () {
    sed -i 's/CONFIG_IFUPDOWN_UDHCPC_CMD_OPTIONS="-R -b"/CONFIG_IFUPDOWN_UDHCPC_CMD_OPTIONS="-R -b -t 1 -T 1 -v"/' ${S}/.config
}

do_install:append:fng() {
    #Ifplugd scripts
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/ifplugd.init ${D}${sysconfdir}/init.d/ifplugd

    install -d ${D}${sysconfdir}/ifplugd
    install -m 0755 ${WORKDIR}/ifplugd.action ${D}${sysconfdir}/ifplugd/ifplugd.action
}

# Prepend as the main busybox package grabs all files left
PACKAGES:prepend = " ${PN}-ifplugd "
INITSCRIPT_PACKAGES:append =" ${PN}-ifplugd "
INITSCRIPT_NAME:${PN}-ifplugd ="ifplugd"
INITSCRIPT_PARAMS:${PN}-ifplugd ="start 90 S ."

FILES:${PN}-ifplugd = " \
    ${sysconfdir}/ifplugd/ifplugd.action \
    ${sysconfdir}/init.d/ifplugd \
    ${sbindir}/ifplugd \
"
