FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
	file://0001-systemd-getty-Removed-default-getty-instance-on-tty1.patch \
"

# We use network manager for the seconorth-wayland distro
#
# Note: Removing networkd directly from the PACKAGECONFIG
# is a "final" decision that always supersedes any additional
# higher-ranked add operations. Since we do have some custom
# layers that still require networkd, we use a workaround
# via an additional variable to remove networkd from the
# PACKAGECONFIG variable. The custom layer then can "remove"
# networkd from the "remove-variable" which in turn "re-adds"
# networkd to the PACKAGECONFIG.
PACKAGECONFIGREMOVE = " networkd "
PACKAGECONFIG:remove = " ${PACKAGECONFIGREMOVE} "

do_install:append () {
	sed -i ${D}/etc/systemd/journald.conf -e 's|^#ReadKMsg=yes$|ReadKMsg=no|'
}
