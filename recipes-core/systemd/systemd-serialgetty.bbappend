# Enable autologin for FNG-System on the serial consoles
do_install:append:fng() {
    find ${D}${systemd_unitdir}/system/ \
        -name 'serial-getty*.service' \
        -exec \
        sed -i -e "s|/sbin/agetty|& --skip-login --autologin root |" {} \;
}
