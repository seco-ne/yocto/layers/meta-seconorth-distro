FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://start_getty.append \
    file://copy_fngs_passwd.append \
"

do_install:append:fng() {
    sed -i ${D}/${sysconfdir}/inittab -e 's|.*getty.*tty1.*|# &|'
    sed -i '2r start_getty.append' ${D}/${base_bindir}/start_getty
    sed -i '21r copy_fngs_passwd.append' ${D}/${base_bindir}/start_getty
    #additional option for autologin
    sed -i '/setsid.*getty/s/-L/$options -L/' ${D}/${base_bindir}/start_getty 
}
