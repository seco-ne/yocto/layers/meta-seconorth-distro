#!/bin/sh

SDMA_SYSFS_PATH="/sys$1/firmware/imx!sdma!sdma-imx7d.bin"
SDMA_FW="/lib/firmware/imx/sdma/sdma-imx7d.bin"

logger "imx-sdma: $SDMA_SYSFS_PATH"

echo 1 > "${SDMA_SYSFS_PATH}/loading"
cat "${SDMA_FW}" > "${SDMA_SYSFS_PATH}/data"
echo 0 > "${SDMA_SYSFS_PATH}/loading"

