FILESEXTRAPATHS:append := "${THISDIR}/${PN}:"

PACKAGECONFIG ??= "disable_automount"

PACKAGECONFIG[disable_automount] = ","

SRC_URI += " \
    file://automount_disable_all \
    file://automount_disable_internal_mmc \
    file://load_sdma_firmware.rules \
    file://load_sdma_firmware.sh \
"

do_install:append() {
    # Check here if the assumed dirs are there, so we don't miss a change because of an upgrade.
    # like we did when mount.blacklist.d was changed to ignorelist
    if [ ! -d ${D}${sysconfdir}/udev/mount.ignorelist.d ];then
        bberror "We assume /etc/udev/mount.ignorelist.d to exist but it is missing"
    fi
    if [ -n "${@bb.utils.contains_any('PACKAGECONFIG', 'disable_automount', '1', '', d)}" ]; then
        install -m 0644 ${WORKDIR}/automount_disable_all ${D}${sysconfdir}/udev/mount.ignorelist.d
    fi

    install -m 0644 ${WORKDIR}/automount_disable_internal_mmc ${D}${sysconfdir}/udev/mount.ignorelist.d
    install -m 0644 ${WORKDIR}/load_sdma_firmware.rules ${D}${sysconfdir}/udev/rules.d
    install -m 0744 ${WORKDIR}/load_sdma_firmware.sh ${D}${sysconfdir}/udev/scripts
}
