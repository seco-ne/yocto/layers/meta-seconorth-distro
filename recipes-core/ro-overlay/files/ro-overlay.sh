#!/bin/sh
#
# Helper script that mounts overlays to specific folders if
# the read-only filesystem feature is used.
#
# Note: The OVERLAY_BASE_DIR and OVERLAID_FOLDERS environment
# variables have to be set outside of the script (e.g. in the
# corresponding service unit).
#

error=0
active_folders=""
check_folders=0
bootdevice="/dev/mmcblk0"

# Determine the bootdevice (eMMC or SD-Card) from the
# cmdline arguments
# shellcheck disable=SC2013
for opt in $(cat /proc/cmdline); do
    arg=$(echo "$opt" | cut -d'=' -f1)
    if [ "$arg" = "root" ]; then
        root_arg=$(echo "$opt" | cut -d'=' -f2)
        bootdevice=$(echo "$root_arg" | sed 's/^\(.*\)p[0-9]*$/\1/')
    fi
done

echo "Info: Selected device is ${bootdevice}"

partition="${bootdevice}p10"

# Check if folders are already overlaid
for folder in ${OVERLAID_FOLDERS}; do
    if mount | grep "lowerdir=${folder}"; then
        echo "Info: ${folder} already mounted; Skipping ..."
    else
        active_folders="${active_folders} ${folder}"
        check_folders=1
    fi
done

# If all folders are already overlaid, we stop further
# processing
if [ "$check_folders" -eq 0 ]; then
    echo "Info: All overlays already mounted; Stopping ..."
    systemd-notify --ready
    exit 0
fi

echo "Check for ${partition} partition ..."

if ! lsblk "$partition"; then
    partition="${bootdevice}p8"

    echo "Check for ${partition} partition ..."

    if ! lsblk "$partition"; then
        echo "Error: Unable to find correct partition; Aborting ..."
        systemd-notify --ready
        exit 1
    fi
fi

# Check if mountpoint for user partition exists
if ! test -d "$OVERLAY_BASE_DIR"; then
    echo "Error: Base directory doesn't exist; Aborting ..."
    systemd-notify --ready
    exit 1
fi

# Check filesystem on user partition
fstype=$(lsblk -f -P "$partition" | sed 's/.*FSTYPE="\([^"]*\)".*/\1/')

if [ ! "$fstype" = "ext4" ]; then
    echo "Error: Invalid filesystem found on partition; Aborting ..."
    systemd-notify --ready
    exit 1
fi

# Mount user partition
if ! df "$OVERLAY_BASE_DIR"; then
    echo "Info: User partition already mounted"
else
    mount -o defaults,sync "$partition" "$OVERLAY_BASE_DIR"
fi

# The shared partition is shadowed by the overlay. To prevent
# this, we unmount the shared partition, mount the overlay
# and then remount the shared partition.
if df /etc/shared; then
    echo "Warning: /etc/shared already mounted"
    umount /etc/shared
fi

# The machine-id is required by various systemd services to run
# correctly. When the overlay is mounted, the mount point of the
# machine-id is shadowed. As a very hacky workaround, we copy
# the machine-id before mounting the overlay and then move it
# back to its original location.
#
# @TODO: Find a better solution for the machine-id
if df /etc/machine-id; then
    echo "Warning: /etc/machine-id already mounted"
    mkdir -p "${OVERLAY_BASE_DIR}/.overlay"
    cp /etc/machine-id "${OVERLAY_BASE_DIR}/.overlay/"
fi

# Check overlayfs mountpoints and mount overlays
for folder in ${active_folders}; do
    readme="${OVERLAY_BASE_DIR}/.overlay/README.txt"
    upper_dir="${OVERLAY_BASE_DIR}/.overlay${folder}"
    work_dir="${OVERLAY_BASE_DIR}/.overlay/work${folder}"

    if ! test -f "$readme"; then
        ln -s /usr/share/ro-overlay/README.txt "$readme"
    fi

    if ! mkdir -p "$upper_dir"; then
        echo "Error: Can't create upperdir; Aborting ..."
        error=1
        break
    fi

    if ! mkdir -p "$work_dir"; then
        echo "Error: Can't create workdir; Aborting ..."
        error=1
        break
    fi

    mount -t overlay -o lowerdir="$folder",upperdir="$upper_dir",workdir="$work_dir" overlay "$folder" || \
        (echo "Error: Unable to mount overlay for $folder; Aborting ..." && exit 1)

    # Move the machine-id back to /etc
    #
    # @TODO: Find a better solution
    if [ "$folder" = "/etc" ]; then
        mv "${OVERLAY_BASE_DIR}/.overlay/machine-id" /etc/machine-id
    fi

    echo "Mounted $upper_dir to $folder"
done

# Remount shared partition
mount /etc/shared

# We have some touchscreen related udev rules, that might be
# located on the overlay. In this case, we have to reload all
# rules and retrigger the touchscreen.
udevadm control --reload-rules && udevadm trigger /sys/class/input/*

if [ "$error" -ne 0 ]; then
    echo "Finished with error"
    systemd-notify --ready
    exit 1
else
    echo "Done"
    systemd-notify --ready
    exit 0
fi
