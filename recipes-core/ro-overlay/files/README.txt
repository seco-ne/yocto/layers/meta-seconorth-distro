This file contains information regarding the read-only filesystem feature in
the SECO North Yocto distro.

Abstract
========
The read-only filesystem feature mounts the root filesystem in read-only mode.
Trying to write or change any files in the root filesystem leads to an error.
The feature makes sure, that the standard applications work correctly in this
mode. However, the feature also prevents changes to any configuration files
(e.g. in /etc). To come around this disadvantage, the ro-overlay service
mounts an overlay to /etc and /opt which re-allows changes without altering
the underlying filesystem. The diff to the filesystem is written to a hidden
folder called .overlay. The default location of the folder is on the last
partition which also acts as the user partition in the SECO North Yocto
distro.

For more information regarding the overlay mechanism see
https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt.
