SUMMARY = "Helper service that mounts writeable overlays when using a read-only filesystem."
HOMEPAGE = "https://seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit systemd

FILESEXTRAPATHS:prepend := "${THISDIR}:files:"

SRC_URI = " \
    file://ro-overlay.service \
    file://ro-overlay.sh \
    file://README.txt \
"

OVERLAY_BASE_DIR ??= "/overlay"
# Note: The path /work can't be overlaid, because it conflicts with the overlay workdir.
OVERLAID_FOLDERS ??= "/etc /opt /root"

do_configure() {
    sed -i ${WORKDIR}/ro-overlay.service \
        -e "s|^Environment=OVERLAY_BASE_DIR=.*$|Environment=OVERLAY_BASE_DIR=\"${OVERLAY_BASE_DIR}\"|" \
        -e "s|^Environment=OVERLAID_FOLDERS=.*$|Environment=OVERLAID_FOLDERS=\"${OVERLAID_FOLDERS}\"|"
}

do_install () {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/ro-overlay.sh ${D}${bindir}/

    install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/ro-overlay.service ${D}${systemd_system_unitdir}/

    # Base directory
    install -d ${D}${OVERLAY_BASE_DIR}

    # Readme
    install -d ${D}${datadir}/${PN}
    install -m 0644 ${WORKDIR}/README.txt ${D}${datadir}/${PN}/
}

FILES:${PN} += " \
    ${bindir}/ro-overlay.sh \
    ${systemd_system_unitdir}/ro-overlay.service \
    ${OVERLAY_BASE_DIR} \
    ${datadir}/${PN}/README.txt \
"

SYSTEMD_SERVICE:${PN} = "ro-overlay.service"
