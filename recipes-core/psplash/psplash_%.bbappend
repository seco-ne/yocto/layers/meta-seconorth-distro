FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://psplash-start \
            file://0001-Add-clear-bg-show-logo-and-show-progress-bar-switche.patch \
            file://0002-Adjust-the-colors-for-the-G-F-logo.patch \
            "

do_install:append() {
    install -d ${D}${bindir}
    install -m 0755 ${WORKDIR}/psplash-start ${D}${bindir}/psplash-start
}
