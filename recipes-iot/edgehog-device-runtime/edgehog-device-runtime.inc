CARGO_BUILD_FLAGS:append = " --features systemd "

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://config.toml \
    file://edgehog-device-runtime.service \
"

RDEPENDS:${PN} = "edgehog-astarte-interfaces"

oe_cargo_fix_env:append(){
    export OPENSSL_NO_VENDOR=1
}

do_install:append () {
    install -d ${D}${sysconfdir}/edgehog
    install -m 0644 ${WORKDIR}/config.toml ${D}${sysconfdir}/edgehog/config.toml

    install -d ${D}${systemd_unitdir}/system
    install -m 0644 ${WORKDIR}/edgehog-device-runtime.service ${D}${systemd_unitdir}/system
}

FILES:${PN} += " \
    ${sysconfdir}/edgehog/* \
    ${systemd_unitdir}/system/edgehog-device-runtime.service \
"

SYSTEMD_SERVICE:${PN} = "edgehog-device-runtime.service"
SYSTEMD_AUTO_ENABLE:${PN} = "disable"

