SUMMARY = "Astarte Interface Descriptions for the Edgehog Device Runtime"
HOMEPAGE = "https://github.com/edgehog-device-manager/edgehog-astarte-interfaces"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

PROTOCOL = "protocol=https"
SRCBRANCH = "branch=main"
SRCREV = "${AUTOREV}"

SRC_URI += " \
    git://github.com/edgehog-device-manager/edgehog-astarte-interfaces;${SRCBRANCH};${PROTOCOL}; \
"

PV = "1.0+git${SRCPV}"
S = "${WORKDIR}/git"

do_install() {
    install -d ${D}/usr/share/edgehog/astarte-interfaces/
    install -m 0644 ${S}/*.json ${D}/usr/share/edgehog/astarte-interfaces/
}

FILES:${PN} = " \
    /usr/share/edgehog/astarte-interfaces/*.json \
"

