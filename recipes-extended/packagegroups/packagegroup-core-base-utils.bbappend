# Currently we are using network-manager's internal dhcpc feature.
# The dhcpcd package also contains a systemd service which starts
# it in parallel, leading to two ip addresses per interface and strange routing
# entries.
# For now we remove it from the packagegroup
RDEPENDS:${PN}:remove = " dhcpcd "
