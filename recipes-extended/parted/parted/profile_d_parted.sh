
# After downgrading parted to 1.8.7 (GPLv2 version), it was discovered
# that in rare cases, parted may stop execution of command and will
# wait for user interaction.

# Add alias to run parted in script mode to prevent this behavior
# and to keep backward compatibility with older fng-install.sh
# scripts.

alias parted='parted -s'
