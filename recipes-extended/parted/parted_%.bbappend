FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
    file://profile_d_parted.sh \
"

do_install:append:fng() {
    install -d ${D}${sysconfdir}/profile.d
    install -d ${D}${sysconfdir}/profile.d/available

    install -m 0755 ${WORKDIR}/profile_d_parted.sh ${D}${sysconfdir}/profile.d/available/parted.sh

    ln -s available/parted.sh ${D}${sysconfdir}/profile.d/parted.sh
}

FILES:${PN}:append = " ${sysconfdir}/profile.d/* "
