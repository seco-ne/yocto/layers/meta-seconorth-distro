SUMMARY = "SECO Northern Europe FNG System logo"
DESCRIPTION = "This package provides logo for FNG System."
HOMEPAGE = "https://www.seco.com"

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"

inherit deploy

RPROVIDES:${PN} += "fnglogo"
PROVIDES += "fnglogo"

SRC_URI = "file://logo_fngsystem_clut224.png"

do_install () {
    install -d ${D}/boot
    install -m 755 ${WORKDIR}/logo_fngsystem_clut224.png ${D}/boot/logo_fngsystem_clut224.png
}

do_deploy () {
    install -d ${DEPLOYDIR}/boot
    install -m 755 ${D}/boot/logo_fngsystem_clut224.png ${DEPLOYDIR}/boot/logo_fngsystem_clut224.png
}

addtask do_deploy after do_install
do_deploy[recrdeptask] += "do_deploy"
do_deploy[cleandirs] = "${DEPLOYDIR}"

FILES:${PN} = "boot/logo_fngsystem_clut224.png"
