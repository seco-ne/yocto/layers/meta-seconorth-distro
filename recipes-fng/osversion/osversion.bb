SUMMARY = "SECO Northern Europe utility to display get version of running OS"
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"


SRC_URI = " \
    file://osversion \
    file://osimageversion \
    "

do_install () {
    install -d ${D}${bindir}/
    install -m 755 ${WORKDIR}/osversion ${D}${bindir}/osversion
    install -m 755 ${WORKDIR}/osimageversion ${D}${bindir}/osimageversion
}

