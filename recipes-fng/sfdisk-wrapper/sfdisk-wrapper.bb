SUMMARY = "SECO Northern Europe sfdisk wrapper to convert the now unsupported -uM parameter used in older fng-install scripts."
HOMEPAGE = "https://www.seco.com"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

SRC_URI = " \
    file://sfdisk-wrapper \
    "

do_install () {
    install -d ${D}${bindir}/
    install -m 755 ${WORKDIR}/sfdisk-wrapper ${D}${bindir}/sfdisk
}


