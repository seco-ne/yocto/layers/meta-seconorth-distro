#
# SPDX-License-Identifier: GPL-2.0-only
#
# DESCRIPTION
# Plugin to deploy Flash-N-Go System to a WiC partition.
# Based on the bootimg-partition plugin.
#
# AUTHORS
# Maciej Borzecki <maciej.borzecki (at] open-rnd.pl>
# Tobias Poganiuch <tobias.poganiuch (at] seco.com>
#

"""
WiC Plugin for Flash-N-Go System
"""

import logging
import os
import re

from glob import glob

from wic import WicError
from wic.engine import get_custom_config
from wic.pluginbase import SourcePlugin
from wic.misc import exec_cmd, get_bitbake_var

logger = logging.getLogger("wic")


class FngsystemPlugin(SourcePlugin):
    """
    Create a partition for SECO Flash-N-Go System.
    """

    name = "fngsystem"

    @classmethod
    def do_configure_partition(
        cls,
        part,
        source_params,
        cr,
        cr_workdir,
        oe_builddir,
        bootimg_dir,
        kernel_dir,
        native_sysroot,
    ):
        """
        Called before do_prepare_partition()
        """

        logger.info("source_params: %s", source_params)
        logger.info("cr: %s", cr)
        logger.info("cr_workdir: %s", cr_workdir)
        logger.info("oe_builddir: %s", oe_builddir)
        logger.info("bootimg_dir: %s", bootimg_dir)
        logger.info("kernel_dir: %s", kernel_dir)
        logger.info("native_sysroot: %s", native_sysroot)

        # Create working directory
        work_dir = "%s/fngsystem.%d" % (cr_workdir, part.lineno)
        install_cmd = "install -d %s" % work_dir
        logger.info("install_cmd: %s", install_cmd)
        exec_cmd(install_cmd)

        # Check image deploy directory
        image_dir = get_bitbake_var("DEPLOY_DIR_IMAGE")
        if not image_dir:
            raise WicError("Couldn't find DEPLOY_DIR_IMAGE, exiting")

        # Validate artifacts from IMAGE_BOOT_FILES variable
        fng_files = None
        for fmt, id in (
            ("_uuid-%s", part.uuid),
            ("_label-%s", part.label),
            (None, None),
        ):
            if fmt:
                var = fmt % id
            else:
                var = ""

            logger.info("var: %s", var)

            fng_files = get_bitbake_var("IMAGE_BOOT_FILES" + var)

            logger.info("fng_files: %s", fng_files)

            if fng_files is not None:
                break

        if fng_files is None:
            raise WicError(
                "No files defined, IMAGE_BOOT_FILES unset for entry #%d" % part.lineno
            )

        logger.info("Files: %s", fng_files)

        # The src_entry/dst_entry pairs are used in the
        # next step to create the actual install_tasks
        deploy_files = []
        for src_entry in re.findall(r"[\w;\-\./\*]+", fng_files):
            if ";" in src_entry:
                dst_entry = tuple(src_entry.split(";"))
                if not dst_entry[0] or not dst_entry[1]:
                    raise WicError("Malformed file entry: %s" % src_entry)
            else:
                dst_entry = (src_entry, src_entry)

            logger.info("Destination entry: %r", dst_entry)
            deploy_files.append(dst_entry)

        # Use the src/dst entries to create installation
        # tasks for the files/folders.
        # The install_task structure is used in
        # do_prepare_partition to create the actual
        # partition.
        cls.install_task = []
        for deploy_entry in deploy_files:
            src, dst = deploy_entry
            if "*" in src:
                # By default install files under their basename
                entry_name_fn = os.path.basename
                if dst != src:
                    # Unless a target name was given, treat the
                    # name as a directory and append a basename
                    entry_name_fn = lambda name: os.path.join(
                        dst, os.path.basename(name)
                    )

                srcs = glob(os.path.join(kernel_dir, src))

                logger.info("Globbed sources: %s", ", ".join(srcs))

                for entry in srcs:
                    src = os.path.relpath(entry, kernel_dir)
                    entry_dst_name = entry_name_fn(entry)
                    cls.install_task.append((src, entry_dst_name))
            else:
                cls.install_task.append((src, dst))

    @classmethod
    def do_prepare_partition(
        cls,
        part,
        source_params,
        cr,
        cr_workdir,
        oe_builddir,
        bootimg_dir,
        kernel_dir,
        rootfs_dir,
        native_sysroot,
    ):
        """
        Steps:
        - Create vfat partition
        - Copy all files listed in IMAGE_BOOT_FILES
        """

        logger.info("cls: %s", cls)
        logger.info("bootimg_dir: %s", bootimg_dir)
        logger.info("kernel_dir: %s", kernel_dir)

        # Set working directory
        work_dir = "%s/fngsystem.%d" % (cr_workdir, part.lineno)

        logger.info("work_dir: %s", work_dir)

        # Check image deploy directory
        image_dir = get_bitbake_var("DEPLOY_DIR_IMAGE")
        if not image_dir:
            raise WicError("Couldn't find DEPLOY_DIR_IMAGE, exiting")

        for task in cls.install_task:
            src_path, dst_path = task
            logger.info("Install %s as %s", src_path, dst_path)
            install_cmd = "install -m 0644 -D %s %s" % (
                os.path.join(kernel_dir, src_path),
                os.path.join(work_dir, dst_path),
            )
            exec_cmd(install_cmd)

        logger.info("Prepare Flash-N-Go System partition in %s", work_dir)
        part.prepare_rootfs(cr_workdir, oe_builddir, work_dir, native_sysroot, False)
