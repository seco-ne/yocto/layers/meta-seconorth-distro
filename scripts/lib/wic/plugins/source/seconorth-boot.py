#
# SPDX-License-Identifier: GPL-2.0-only
#
# DESCRIPTION
# This implements the 'seconorth-boot' source plugin class for
# 'wic'. The plugin creates an image of boot partition, copying over
# files listed in rootfs/boot/.
# The implementation is mainly a copy of the bootimg-partition.py
# plugin (poky layer).
#
# AUTHORS
# Maciej Borzecki <maciej.borzecki (at] open-rnd.pl>
# Felix Gerking <felix.gerking /at] seco.com>
#

import logging
import os
import re

from glob import glob

from wic import WicError
from wic.pluginbase import SourcePlugin
from wic.misc import exec_cmd, get_bitbake_var

logger = logging.getLogger('wic')

class SeconorthPartitionPlugin(SourcePlugin):
    """
    Create an image of boot partition, copying over files
    listed in IMAGE_BOOT_FILES bitbake variable.
    """

    name = 'seconorth-boot'

    @classmethod
    def do_configure_partition(cls, part, source_params, cr, cr_workdir,
                             oe_builddir, bootimg_dir, kernel_dir,
                             native_sysroot):
        """
        Called before do_prepare_partition(), create u-boot specific boot config
        """
        hdddir = "%s/boot.%d" % (cr_workdir, part.lineno)
        install_cmd = "install -d %s" % hdddir
        exec_cmd(install_cmd)

        # In contrast to the default yocto artifacts path, our boot artifacts
        # are stored at rootfs/boot, so override kernel_dir respectively
        kernel_dir = os.path.join(get_bitbake_var('IMAGE_ROOTFS') + "/boot/")
        if not kernel_dir:
            raise WicError("Couldn't find DEPLOY_DIR_IMAGE, exiting")
        logger.debug('Boot files path: %s', kernel_dir)

        # Stage all files in rootfs/boot/ for deployment
        boot_files = None
        for entry in os.listdir(kernel_dir):
            if os.path.isfile(os.path.join(kernel_dir, entry)):
                # Convert to boot_files to string if a file exists
                if boot_files is None:
                    boot_files = ""
                boot_files = boot_files + "{} ".format(entry)


        if boot_files is None:
            raise WicError('No boot files defined, IMAGE_BOOT_FILES unset '
                    'for entry #%d' % part.lineno)

        logger.debug('Boot files: %s', boot_files)

        # list of tuples (src_name, dst_name)
        deploy_files = []
        for src_entry in re.findall(r'[\w;\-\./\*]+', boot_files):
            if ';' in src_entry:
                dst_entry = tuple(src_entry.split(';'))
                if not dst_entry[0] or not dst_entry[1]:
                    raise WicError('Malformed boot file entry: %s' % src_entry)
            else:
                dst_entry = (src_entry, src_entry)

            logger.debug('Destination entry: %r', dst_entry)
            deploy_files.append(dst_entry)

        cls.install_task = []
        for deploy_entry in deploy_files:
            src, dst = deploy_entry
            if '*' in src:
                # by default install files under their basename
                entry_name_fn = os.path.basename
                if dst != src:
                    # unless a target name was given, then treat name
                    # as a directory and append a basename
                    entry_name_fn = lambda name: \
                                    os.path.join(dst,
                                                 os.path.basename(name))

                srcs = glob(os.path.join(kernel_dir, src))

                logger.debug('Globbed sources: %s', ', '.join(srcs))
                for entry in srcs:
                    src = os.path.relpath(entry, kernel_dir)
                    entry_dst_name = entry_name_fn(entry)
                    cls.install_task.append((src, entry_dst_name))
            else:
                cls.install_task.append((src, dst))


    @classmethod
    def do_prepare_partition(cls, part, source_params, cr, cr_workdir,
                             oe_builddir, bootimg_dir, kernel_dir,
                             rootfs_dir, native_sysroot):
        """
        Called to do the actual content population for a partition i.e. it
        'prepares' the partition to be incorporated into the image.
        In this case, does the following:
        - sets up a vfat partition
        - copies all files listed in IMAGE_BOOT_FILES variable
        - SECO specific modifications for kernel boot
        """
        hdddir = "%s/boot.%d" % (cr_workdir, part.lineno)

        # Override kernel_dir as in do_configure_partition
        kernel_dir = os.path.join(get_bitbake_var('IMAGE_ROOTFS') + "/boot/")
        if not kernel_dir:
            raise WicError("Couldn't find DEPLOY_DIR_IMAGE, exiting")

        logger.debug('Kernel dir: %s', kernel_dir)

        for task in cls.install_task:
            src_path, dst_path = task
            logger.debug('Install %s as %s', src_path, dst_path)
            install_cmd = "install -m 0644 -D %s %s" \
                          % (os.path.join(kernel_dir, src_path),
                             os.path.join(hdddir, dst_path))
            exec_cmd(install_cmd)

        # Search for kernel and boot.cfg file in wic tmp destination
        kernel_file = None
        boot_cfg_file = None
        for task in cls.install_task:
            src_path, dst_path = task
            if "Image" in os.path.basename(dst_path):
                kernel_file = os.path.basename(dst_path)
                logger.debug('Set %s as kernel in boot.cfg', kernel_file)
            if "boot.cfg" in os.path.basename(dst_path):
                boot_cfg_file = dst_path
        if not kernel_file:
            raise WicError("Couldn't find a valid kernel file in %s, exiting" \
                            % kernel_dir)
        if not boot_cfg_file:
            raise WicError(
                    "Couldn't find a valid boot.cfg file in %s, exiting" \
                            % kernel_dir)

        # Write a file containing the kernel file name as text,
        # as fng-install does
        kernel_txt_file = os.path.join(os.path.dirname(dst_path), "kernel")
        with open(os.path.join(hdddir, kernel_txt_file), "w") as kernel_txt:
            kernel_txt.write("%s\n" % kernel_file)

        # Set the rootfs to the device WIC image is installed on
        rootfs_device = get_bitbake_var('WIC_ROOTFS_DEVICE')
        logger.debug('WIC_ROOTFS_DEVICE: %s', rootfs_device)
        if not rootfs_device:
            raise WicError("Bitbake variable WIC_ROOTFS_DEVICE is not defined,"
            " exiting")
        rootfs_node = "/dev/%sp8" % rootfs_device
        logger.debug('Set rootfs parameter in cmdline: %s', rootfs_node)

        # Modify boot.cfg file in wic tmp destination. As in fng-install we
        # set the corresponding kernel and rootfs partition.
        with open(os.path.join(hdddir, boot_cfg_file), "r") as boot_cfg:
            boot_cfg_lines = boot_cfg.readlines()
        with open(os.path.join(hdddir, boot_cfg_file), "w") as boot_cfg:
            for line in boot_cfg_lines:
                if "kernel=" in line:
                    boot_cfg.write(re.sub(r'kernel=linuximage',
                                'kernel=%s' % kernel_file,line))
                elif "cmdline" in line:
                    boot_cfg.write(re.sub(r'root=/dev/\w*',
                        'root=%s' % rootfs_node, line))
                else:
                    boot_cfg.write(line)

        logger.debug('Prepare boot partition using rootfs in %s', hdddir)
        part.prepare_rootfs(cr_workdir, oe_builddir, hdddir,
                            native_sysroot, False)
